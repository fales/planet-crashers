@tool
extends Area2D
class_name Hurtbox
## An [Area2D] representing a region of a player or enemy that can take damage

## If true this hurtbox will be detectable by player hitboxes. Otherwise it will be detectable by 
## enemy hitboxes
@export var is_enemy = true:
	set(val):
		is_enemy = val
		collision_layer = Globals.ENEMY_COLLISION_LAYER if is_enemy else Globals.PLAYER_COLLISION_LAYER


func _init():
	input_pickable = false
	monitoring = false
	collision_mask = 0
	is_enemy = is_enemy


func _ready():
	if not Engine.is_editor_hint():
		assert(owner != null, "Hurtbox needs an owner")
		assert(owner.has_method("damage"), "Hurtbox owner shuold implement damage method")
