extends VBoxContainer

signal back_to_main_menu
signal start_game

var timeout_join_seconds = 20
var _joined = false

func _ready():
	_set_status()
	$buttons/back.pressed.connect(_on_back_pressed)
	$buttons/refresh.pressed.connect(_refresh_lobbies)
	$buttons/join.pressed.connect(_on_join_pressed)
	multiplayer.connected_to_server.connect(_success)
	multiplayer.connection_failed.connect(_fail)
	_joined = false
	$ItemList.item_selected.connect(_lobby_item_selected)
	_refresh_lobbies()

func _refresh_lobbies():
	var find = {
		"include_player_count": true,
		"only_my_lobbies": false,
		"constraints": {
			"state": [W4GD.matchmaker.LobbyState.NEW, W4GD.matchmaker.LobbyState.IN_PROGRESS]
		}
	}
	W4GD.matchmaker.find_lobbies(find).then(_show_lobbies).async()

func _show_lobbies(result, max_age_minutes=60, show_without_gameserver=false):
	$ItemList.clear()
	var lobbies : Array = result.as_array()
	lobbies.sort_custom(func(a, b): return a.created_at > b.created_at)
	var idx = 0
	for l in lobbies:
		if not show_without_gameserver and not "gameserver" in l or l["gameserver"] == null:
			continue
		if not l or l.hidden:
			continue
		if "creator_id" in l and l["creator_id"] == Net.get_id():
			continue
		var creator_name = ""
		if "props" in l and "creator_name" in l["props"] and len(l["props"]["creator_name"]) > 0:
			creator_name = l["props"]["creator_name"] + " "
		var time = Time.get_datetime_dict_from_unix_time(l.created_at)
		var time_formatted = "%d-%d-%d %d:%d" % [time["year"], time["month"], time["day"], time["hour"], time["minute"]]
		var age_minutes = round((Time.get_unix_time_from_system() - l.created_at) / 60)
		if age_minutes > max_age_minutes:
			continue
		var lobby_description = "%d/%d %s%s (%s)" % [l.player_count, l.max_players, creator_name, time_formatted, l.id]
		$ItemList.add_item(lobby_description)
		$ItemList.set_item_metadata(idx, l.id)
		$ItemList.set_item_tooltip_enabled(idx, false)
		idx += 1

func _lobby_item_selected(idx):
	var id = $ItemList.get_item_metadata(idx)
	$game.text = str(id)

func _on_join_pressed():
	_joined = false
	var addr = $game.text.strip_edges()
	if len(addr) < 1:
		addr = "localhost"
	var e = await Net.join_game(addr)
	if e != OK:
		_set_status("Error: %s [%d]" % [error_string(e), e])
		_set_buttons_enabled(true)
	else:
		_set_status("Server found, joining...", true)
		_set_buttons_enabled(false)
	get_tree().create_timer(timeout_join_seconds).timeout.connect(_timeout)

func _timeout():
	if not _joined:
		_fail()

func _fail():
	Net.disconnect_from_game()
	_set_status("Failed to connect")
	_set_buttons_enabled(true)

func _success():
	_joined = true
	start_game.emit()
	_set_buttons_enabled(true)

func _on_back_pressed():
	back_to_main_menu.emit()

func _set_buttons_enabled(enabled : bool):
	$buttons/back.disabled = !enabled
	$buttons/join.disabled = !enabled

	var anim_duration = 0.4
	if enabled:
		get_tree().create_tween().tween_property($buttons/progress, "modulate",
			Color(1, 1, 1, 0), anim_duration)
	else:
		get_tree().create_tween().tween_property($buttons/progress, "modulate",
			Color(1, 1, 1, 1), anim_duration)

func _set_status(msg="", success=false):
	var status = $status
	status.text = msg
	if not success:
		status.set("theme_override_colors/font_color", Color.RED)
	else:
		status.set("theme_override_colors/font_color", Color.LAWN_GREEN)
