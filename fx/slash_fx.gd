extends AnimatedSprite2D

const variants = ["slash_1", "slash_2"]

func play_fx():
	flip_h = [true, false].pick_random()
	var to_play = variants.pick_random()
	play(to_play)
	await animation_finished
	queue_free()
