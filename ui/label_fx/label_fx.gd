extends RichTextLabel

func _ready():
	mouse_entered.connect(_mouse_entered)
	focus_entered.connect(_focus_entered)
	get_v_scroll_bar().scrolling.connect(_scrolling)

func _mouse_entered():
	if is_visible_in_tree():
		grab_focus()

func _focus_entered():
	$FocusEnterSFX.play()

func _scrolling():
	print("Scroll")
