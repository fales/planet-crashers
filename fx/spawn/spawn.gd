extends Node2D

var spawn_child : Node2D

var spawn_height = 500
var spawn_duration = 3.0
var spawn_step_duration = 1.0
var shadow_max_frame = 3

var spawn_final_color
var tween

func _ready():
	if not multiplayer.is_server():
		return

	var parent = get_parent()
	parent.position.y -= spawn_height
	if "is_controllable" in parent:
		shadow_max_frame -= 2

	if "skip_spawn_animation" in parent and parent.skip_spawn_animation:
		_finish_spawn()
		return

	spawn_final_color = parent.modulate
	parent.modulate = parent.modulate.darkened(1.0)
	var shadow = $shadow as Node2D
	var shadow_initial_alpha = shadow.self_modulate.a
	shadow.self_modulate.a = 0

	tween = get_tree().create_tween()
	tween.tween_property(
		shadow, "self_modulate:a", shadow_initial_alpha, spawn_step_duration)
	tween.parallel().tween_property(
		parent, "position",
		Vector2(parent.position.x, parent.position.y+spawn_height),
		spawn_duration).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN)
	tween.parallel().tween_property(
		parent, "modulate", spawn_final_color,
		spawn_duration).set_ease(Tween.EASE_IN)
	tween.tween_callback($splash.play.bind("default"))
	tween.tween_property(
		parent, "position",
		Vector2(parent.position.x, parent.position.y+spawn_height),
		0.5
	)
	tween.tween_callback(_finish_spawn)
	get_tree().create_timer(spawn_step_duration * 2.1).timeout.connect(_play_spawn_sfx)
	get_tree().create_timer(spawn_step_duration * 2.3).timeout.connect(_play_spawn_start_sfx)
	get_tree().create_timer(spawn_step_duration).timeout.connect(_spawn_step)

func _play_spawn_start_sfx():
	Audio.sfx_play.rpc("res://assets/sfx/spawn/woosh.wav")

func _play_spawn_sfx():
	Audio.sfx_play.rpc("res://assets/sfx/spawn/jet_woosh.wav", Audio.Bus.GAMEPLAY, {"volume_db": -10.0})

func _spawn_step():
	var shadow = $shadow as AnimatedSprite2D
	if not shadow:
		return
	if shadow.frame < shadow_max_frame:
		shadow.frame += 1
	if shadow.frame < shadow_max_frame:
		get_tree().create_timer(spawn_step_duration).timeout.connect(_spawn_step)

func _finish_spawn():
	var parent = get_parent()
	var shadow = $shadow as AnimatedSprite2D
	remove_child(shadow)
	shadow.show_behind_parent = true
	parent.add_child.call_deferred(shadow)
	queue_free()

func _exit_tree():
	if tween:
		tween.kill()
