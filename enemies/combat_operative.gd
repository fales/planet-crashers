extends Enemy

# Currently both combat_operative.tscn and suicide_drone.tscn use this script since they seem to have
# the same behavior

@export var DAMAGE : int = 10

@onready var hurtbox : Hurtbox = $visuals/hurtbox
@onready var hitbox : HitboxArea = $visuals/hitbox

var _last_pos : Vector2 = position

func _physics_process(_delta):
	$flocking.move_speed = SPEED
	_update_facing()


func _update_facing():
	var facing = sign(position.x - _last_pos.x)
	_last_pos = position
	if facing != 0:
		$visuals.scale.x = sign(facing)

func die():
#	if not is_multiplayer_authority():
#		return
	hitbox.set_deferred("monitoring", false)
	$visuals/sprite.stop()
	await get_tree().create_timer(0.3).timeout
	for i in 3:
		var boom_fx = preload("res://fx/explosion.tscn").instantiate()
		get_parent().add_child(boom_fx)
		boom_fx.scale = Vector2.ONE * randf_range(0.5, 1.0)
		boom_fx.global_position = $visuals.global_position + Vector2(randf_range(-16,16),randf_range(-8,8))
	super()


func _on_hitbox_target_acquired(_target):
	if not is_multiplayer_authority():
		return
	hitbox.deal_damage(DAMAGE, {"direction" : global_position.direction_to(_target.global_position)})
	die()

func _set_type(t : Types):
	super(t)
	if !_is_ready:
		await ready
	var mat : Material
	match t:
		Types.RED:
			mat = preload("res://enemies/recolors/red.material")
		Types.BLUE:
			mat = preload("res://enemies/recolors/blue.material")
		Types.GRAY:
			mat = preload("res://enemies/recolors/gray.material")
		Types.GREEN:
			mat = null
	$visuals/sprite.material = mat

func _update_params():
	MAX_HP = GameConfig.CombatOperative_MAX_HP
	SPEED = GameConfig.CombatOperative_SPEED * Globals.PIXELS_PER_UNIT
	SP_DROP_AMOUNT = GameConfig.CombatOperative_SP_DROP_AMOUNT
	HP_DROP_AMOUNT = GameConfig.CombatOperative_HP_DROP_AMOUNT
	DAMAGE = GameConfig.CombatOperative_DAMAGE
	super()
