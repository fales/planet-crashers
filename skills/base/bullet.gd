@tool
extends HitboxArea
class_name Bullet

var _damage := 1
var _velocity := Vector2.RIGHT
var _custom_data := {}

@onready var sprite = $sprite

func setup(global_pos : Vector2, velocity: Vector2, damage: int, custom_data : Dictionary ):
	global_position = global_pos
	_damage = damage
	_velocity = velocity
	sprite.rotation = velocity.angle()
	_custom_data = custom_data.duplicate()
	var fx = custom_data.get("effect")
	if fx:
		_set_effect(fx)


func _physics_process(delta: float) -> void:
	if Engine.is_editor_hint():
		set_physics_process(false)
		return
	position += _velocity * delta


func _on_target_acquired(target : Hurtbox) -> void:
	target.owner.damage(_damage, _custom_data)
	queue_free()


func _on_visible_on_screen_notifier_2d_screen_exited() -> void:
	queue_free()

func _set_effect(fx : SkillDB.Effects):
	var mat : Material
	match fx:
		SkillDB.Effects.BLAZING:
			sprite.animation = "blazing"
		SkillDB.Effects.CRYO:
			sprite.animation = "cryo"
		SkillDB.Effects.CORROSION:
			sprite.animation = "corrosion"
		SkillDB.Effects.NORMAL:
			sprite.animation = "normal"
	sprite.play()
