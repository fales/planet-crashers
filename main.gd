extends Control

var LEVEL = preload("res://level/level.tscn")
var MENU := {
	MAIN = preload("res://ui/menus/main/menu.tscn"),
	LOGIN = preload("res://ui/menus/login/login.tscn"),
	JOIN = preload("res://ui/menus/join/join.tscn")
}

var menu_change_tween_time = 0.5
var x_adjust = 5000;
var initial_content = null
var active_menu = null
var playing = false

@export
var game_container : SubViewportContainer
@export
var game_viewport : SubViewport

var subtitles = preload("res://ui/menus/main/subtitles.gd").new()

func _ready():
	Events.game_error.connect(_game_error)
	Events.game_end.connect(_stop_game)
	multiplayer.server_disconnected.connect(_stop_game)
	initial_content = $menu/content.duplicate()
	_switch_menu()
	Events.game_hidden.connect(Audio.music_play.bind("res://assets/music/dpfi-espantomusic.ogg"))
	Events.game_shown.connect(Audio.music_stop.bind(3.0))
	Events.game_begin.connect(Audio.music_play.bind("res://assets/music/mediacharger_power_up.ogg"))
	Events.game_over.connect(_game_over)
	Audio.music_play("res://assets/music/dpfi-espantomusic.ogg")
	if (randf() > 0.3):
		$menu/title/margin/subtitle.text = " " + subtitles.subtitles.pick_random() + " "


func _game_error(msg : String):
	print("Error: ", msg)
	Globals.popup(msg)
	_stop_game()


func _game_over(players : int, players_alive : int, round : int):
	Audio.music_stop(3.0)


func _input(event):
	if playing and event.is_action_pressed("quit"):
		_stop_game()

func _stop_game():
	if !playing:
		return

	var tween := create_tween()
	tween.tween_property(game_container, "modulate", Color.BLACK,
		menu_change_tween_time * 3) \
		.set_ease(Tween.EASE_IN) \
		.set_trans(Tween.TRANS_CUBIC)
	tween.tween_callback(_hide_game_callback)

	var menu = $menu
	menu.modulate = Color.BLACK
	var tween_in := create_tween()
	tween_in.tween_property(menu, "modulate", Color.WHITE,
		menu_change_tween_time * 5) \
		.set_ease(Tween.EASE_IN) \
		.set_trans(Tween.TRANS_CUBIC)

	playing = false
	Events.game_hidden.emit()
	Net.disconnect_from_game()

func _hide_game_callback():
	$menu.show()
	game_container.hide()
	for c in game_viewport.get_children():
		c.queue_free()
	_switch_menu()

func _host_game_pressed():
	var e := await Net.host_game()
	if e != OK:
		return Globals.popup("Error: %s (%d)" % [error_string(e), e])
	GameConfig.updated.connect(_start_game)
	GameConfig.update()

func _start_game_pressed():
	_start_game()

func _join_game_pressed():
	_switch_menu(MENU.JOIN)

func _start_game():
	if playing:
		return
	if GameConfig.updated.is_connected(_start_game):
		GameConfig.updated.disconnect(_start_game)

	for c in game_viewport.get_children():
		c.queue_free()

	var level_instance = LEVEL.instantiate()
	game_viewport.add_child(level_instance)
	game_container.show()
	game_container.modulate = Color.BLACK
	var tween_in := create_tween()
	tween_in.tween_property(game_container, "modulate", Color.WHITE,
		menu_change_tween_time * 5) \
		.set_ease(Tween.EASE_IN) \
		.set_trans(Tween.TRANS_CUBIC)

	_menu_content_animate_out()

	var menu = $menu
	var tween := create_tween()
	tween.tween_property(menu, "modulate", Color.BLACK,
		menu_change_tween_time * 3) \
		.set_ease(Tween.EASE_IN) \
		.set_trans(Tween.TRANS_CUBIC)
	tween.tween_callback($menu.hide)

	playing = true
	Events.game_shown.emit()

func _switch_menu(scene=MENU.MAIN):
	Net.disconnect_from_game()

	if (Globals.is_headless()):
		_host_game_pressed()
		return

	if scene != MENU.LOGIN and not Net.is_logged_in():
		return await _switch_menu(MENU.LOGIN)

	if scene == active_menu:
		return

	_menu_content_animate_out()
	_menu_content_animate_in(scene)

func _menu_content_animate_in(scene):
	var new_content = initial_content.duplicate()
	var s = scene.instantiate()
	if s.has_signal("back_to_main_menu"):
		s.back_to_main_menu.connect(_switch_menu.bind(MENU.MAIN))
	if s.has_signal("start_game"):
		s.start_game.connect(_start_game_pressed)
	if s.has_signal("join_game"):
		s.join_game.connect(_join_game_pressed)
	if s.has_signal("host_game"):
		s.host_game.connect(_host_game_pressed)
	new_content.add_child(s)
	new_content.position.x += x_adjust
	$menu/title.add_sibling(new_content)

	var tween_new := create_tween()
	tween_new.tween_property(new_content, "position",
		Vector2(new_content.position.x-x_adjust, new_content.position.y),
		menu_change_tween_time) \
		.set_ease(Tween.EASE_IN) \
		.set_trans(Tween.TRANS_BOUNCE)

	active_menu = scene

func _menu_content_animate_out():
	if not $menu.has_node("content"):
		return
	var content := $menu/content
	content.name = "content_old"
	var tween_old := create_tween()
	tween_old.tween_property(content, "position", Vector2(-x_adjust,
		content.position.y), menu_change_tween_time) \
		.set_ease(Tween.EASE_OUT) \
		.set_trans(Tween.TRANS_BOUNCE)
	tween_old.tween_callback(content.queue_free)

	active_menu = null
