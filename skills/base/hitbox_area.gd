@tool
class_name HitboxArea
extends Area2D
## An Area2D that can deal damage to to the objects that overlap with it
##
## Can inform about targets entering and leaving the area through [signal target_acquired] and
## [signal target_lost]
## Can handle dealing damage to a random target with [method deal_damage_to_random_target]
## 
## If you need something that can be repositioned and check for collisions in the same frame
## Use [Hitbox] or its derived classes

## Contains the list of currently acquired targets
## 
## GDScript doesn't have a Set datatype yet so a Dictionary is used
var targets : Dictionary = {}

signal target_acquired( target : Area2D )
signal target_lost( target : Area2D )


func _init():
	monitorable = false
	is_enemy = is_enemy # Setter doesn't seem to trigger automatically as in Godot 3


func _ready():
	area_entered.connect(_on_area_entered)
	area_exited.connect(_on_area_exited)


## Sets the proper collision mask depending on what objects it should target
@export var is_enemy = true :
	set(value):
		is_enemy = value
		# If enemy target players else target enemies
		collision_mask = Globals.PLAYER_COLLISION_LAYER if is_enemy else Globals.ENEMY_COLLISION_LAYER


## Forces a shapecast update and calls the damage method on the owner of any
## [Hurtbox] instances it hits
## [param amount]: amount of damage to cause
## [param custom_data]: A dictionary with any extra info about the damage. Currently used for 
## status effects and damage type
func deal_damage( amount : int, custom_data:= {} ) -> int:
	for target in targets:
		target.owner.damage(amount, custom_data)
	return targets.size()


## Checks if a target candidate is valid
##
## A valid target shuold have an owner set
## Its owner should have an hp property
## Its owner should have hp above 0
## Its owner should not be marked as dead
func is_valid_target(target : Area2D) -> bool:
	if not "owner" in target:
		return false
	if not "hp" in target.owner:
		return false
	if target.owner.hp <= 0:
		return false
	if "dead" in target.owner and target.owner.dead:
		return false
	return true


## Convinience method. Same as [method deal_damage] but will only hit one random target
func deal_damage_to_random_target( amount : int, custom_data:= {} ) -> bool:
	if targets.is_empty():
		return false
	var target = targets.keys().pick_random()
	target.owner.damage(amount, custom_data)
	return true


func _on_area_entered( area : Area2D ):
	if is_valid_target(area):
		targets[area] = true
		target_acquired.emit(area)


func _on_area_exited( area : Area2D ):
	targets.erase(area)
	target_lost.emit(area)
