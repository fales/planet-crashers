@tool
extends Node2D
class_name PointBar
## Draws a rectangular bar with animated changes
##
## Useful for hitpoints, mana, and any numerical stat that should be displayed as a bar

## Color of the empty section of the bar
@export var bg_color : Color = Color(0.1,0.1,0.1) :
	set(val):
		bg_color = val
		_update_rects()

## Color of the filled section of the bar
@export var fg_color : Color = Color.CRIMSON:
	set(val):
		fg_color = val
		_update_rects()

## Color of the updating section of the bar when the fill value changes
@export var anim_color : Color = Color.WHITE:
	set(val):
		anim_color = val
		_update_rects()

## Maximum value (full bar)
@export var max_value : int = 100:
	set(val):
		max_value = val
		value = clamp(value, 0, max_value)
		_anim_value = clamp(_anim_value, 0, max_value)
		_update_rects()

## Current value. Clamped between 0 and [member max_value]
@export var value : int = 50:
	set(val):
		value = clamp(val, 0, max_value)
		_update_rects()

## Width and height of the (full) bar
@export var size : Vector2i = Vector2i(24,3):
	set(val):
		size = val
		_update_rects()

## Any value higher than 1 will cause tick marks to appear splitting the bar in sections
@export var subdivisions : int = 1:
	set(val):
		if val < 1:
			return
		subdivisions = val
		_update_rects()

## Bar will start flashing below this value
## This is an absolute value, not a percentage and won't be automatically remapped
## if max_value changes
@export var warning_below: int = 0:
	set(val):
		warning_below = val
		_update_rects()


## Bar will start flashing above this value
## This is an absolute value, not a percentage and won't be automatically remapped
## if max_value changes
@export var warning_above: int = int(INF)-1: #Hack to get MAX_INT
	set(val):
		warning_above = val
		_update_rects()

var _anim_value : int = value
var _bg_rect : Rect2i
var _fg_anim_rect : Rect2i
var _fg_rect : Rect2i
var _fg_color := fg_color
var _accum: float = 0.0
var _tick_marks : PackedVector2Array = PackedVector2Array()

func _ready():
	z_index = Globals.IN_GAME_UI_Z_INDEX
	_update_rects()


func _physics_process(delta):
	if value > warning_above or value < warning_below:
		_accum = wrapf(_accum + 4.0 * delta, 0.0, 1.0)
		_fg_color = lerp(fg_color, anim_color, _accum)
		_update_rects()
	else:
		_accum = 0
		_fg_color = fg_color
	if _anim_value != value:
		var new_value : int = lerp(_anim_value, value, 0.2)
		if new_value == _anim_value:
			_anim_value = value
		else:
			_anim_value = new_value
		_update_rects()


func _draw():
	draw_rect(_bg_rect, bg_color)
	# If decreasing show animated part in anim_color else show animated part in fg_color
	if _anim_value <= value:
		draw_rect(_fg_rect, anim_color)
		draw_rect(_fg_anim_rect, _fg_color)
	else:
		draw_rect(_fg_anim_rect, anim_color)
		draw_rect(_fg_rect, _fg_color)
	
	@warning_ignore("integer_division")
	for i in _tick_marks.size() / 2:
		draw_line(_tick_marks[2*i], _tick_marks[2*i+1], fg_color.darkened(0.5), 1)

func _update_rects():
	_bg_rect = Rect2i(-0.5 * size, size)
	@warning_ignore("narrowing_conversion")
	_fg_rect = Rect2(- 0.5 * size, Vector2i(float(value)/max_value * size.x, size.y) )
	@warning_ignore("narrowing_conversion")
	_fg_anim_rect = Rect2(- 0.5 * size, Vector2i(float(_anim_value)/max_value * size.x, size.y) )
	_tick_marks = PackedVector2Array()
	if subdivisions > 1:
		for i in range(1,subdivisions):
			@warning_ignore("integer_division")
			_tick_marks.append(Vector2(-0.5 * size.x + i * size.x/subdivisions, ceil(-0.5 * size.y)))
			@warning_ignore("integer_division")
			_tick_marks.append(Vector2(-0.5 * size.x + i * size.x/subdivisions, ceil(0.5 * size.y)))
	queue_redraw()
	pass
