extends Skill

@export var DAMAGE : int = 20
@export var RADIUS : float = 2.5 * Globals.PIXELS_PER_UNIT

@onready var bg : AnimatedSprite2D = $bg
@onready var fg : AnimatedSprite2D = $fg

var _anim_name = "slash_green"
var _anim_length = 1.0

func _ready():
	bg.hide()
	fg.hide()
	bg.animation_finished.connect(bg.hide)
	fg.animation_finished.connect(fg.hide)
	_anim_length = bg.sprite_frames.get_frame_count(_anim_name) / float(bg.sprite_frames.get_animation_speed(_anim_name))
	super()


func _launch():
	bg.show()
	fg.show()
	bg.play(_anim_name)
	fg.play(_anim_name)
	($hitbox as Hitbox).deal_damage(DAMAGE, {"type" : "slash"})
	pass


func _update_params():
	DAMAGE = GameConfig.CircleSlash_DAMAGE
	COOLDOWN = max( GameConfig.CircleSlash_COOLDOWN, _anim_length )
	SP_COST = GameConfig.CircleSlash_SP_COST
	RADIUS = GameConfig.CircleSlash_RADIUS * Globals.PIXELS_PER_UNIT
	$hitbox.shape.radius = RADIUS
	bg.scale = Vector2.ONE * (RADIUS / 80.0) # Divisor based on current asset dimensions
	fg.scale = bg.scale


func _update_color():
	_anim_name = "slash" + color_suffix
