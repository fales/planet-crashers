class_name  SkillDB
extends RefCounted
## Holds static info about skills
##
## Currently used for weapon and status effect info.


## The current iteration of the game limits the skills usable as base attack to a limited number of
## "weapons". This enum holds the wepon types
enum Weapons { SWORD, CHAIN, BATON, SHOTGUN, RIFLE }

## Skills can optionally apply status effects. This enum holds the current status effects
enum Effects { NORMAL, BLAZING, CORROSION, CRYO }

## Contains data for every weapon
## scene_path: The path to the Skill scene for that weapon
## name: to use were strings identifiers make more sense than enums (e.g: animation names, debugging)
const weapon_info = {
	Weapons.SWORD: {
		"scene_path" : "res://skills/electro_sword.tscn",
		"name" : "sword",
	},
	Weapons.SHOTGUN : {
		"scene_path": "res://skills/shotgun.tscn",
		"name": "shotgun",
	},
	Weapons.BATON: {
		"scene_path" : "res://skills/baton.tscn",
		"name" : "baton",
	},
	Weapons.CHAIN: {
		"scene_path" : "res://skills/chain.tscn",
		"name" : "chain"
	},
	Weapons.RIFLE: {
		"scene_path" : "res://skills/rifle.tscn",
		"name" : "rifle"
	} 
}

## To use were strings identifiers make more sense than enums (e.g: animation names, debugging)
const effect_names = {
	Effects.NORMAL : "normal",
	Effects.BLAZING : "blazing",
	Effects.CORROSION : "corrosion",
	Effects.CRYO : "cryo",
}

static var weapon_rng: WeightedRandom = WeightedRandom.new([1,1,1,1,1])

static var effect_rng: WeightedRandom = WeightedRandom.new([0.4,0.2,0.2,0.2])

static func update_weights():
	weapon_rng = WeightedRandom.new([
		GameConfig.Sword_DROP_CHANCE,
		GameConfig.Chain_DROP_CHANCE,
		GameConfig.Baton_DROP_CHANCE,
		GameConfig.Shotgun_DROP_CHANCE,
		GameConfig.Rifle_DROP_CHANCE,
	])

	effect_rng = WeightedRandom.new([
		GameConfig.Normal_DROP_CHANCE,
		GameConfig.Blazing_DROP_CHANCE,
		GameConfig.Corrosion_DROP_CHANCE,
		GameConfig.Cryo_DROP_CHANCE,
		
	])
