extends Item

@export var HP_HEAL : int = 10

func _apply_effect(target: Area2D) -> void:
	target.owner.damage(-HP_HEAL)
