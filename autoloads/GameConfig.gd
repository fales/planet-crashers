extends Node
## A list of every gameplay parameter game designers might want to adjust
## If possible use the format [code]var <ClassName|SceneName>_<variable name>[/code] for consistency
## Keep all params related to the same class together

# res://items/base/Item.gd
var Item_DESPAWN_TIME : float = 3.0

# res://enemies/base/enemy.tscn
var Nothing_DROP_CHANCE : float= 0.8
var PowerCell_DROP_CHANCE : float= 0.20
var StimPack_DROP_CHANCE : float = 0.15
var Weapon_DROP_CHANCE :float = 0.05
var RedVariant_SPAWN_CHANCE : float = 0.2 
var GreenVariant_SPAWN_CHANCE : float = 0.1
var BlueVariant_SPAWN_CHANCE : float = 0.2
var GrayVariant_SPAWN_CHANCE : float = 0.5


# res://items/weapon_drop.tscn
var Sword_DROP_CHANCE : float = 0.2
var Chain_DROP_CHANCE : float = 0.2
var Baton_DROP_CHANCE : float = 0.2
var Shotgun_DROP_CHANCE : float = 0.2
# Disabled due to performance issue
var Rifle_DROP_CHANCE : float = 0.0

var Normal_DROP_CHANCE : float = 0.4
var Blazing_DROP_CHANCE : float = 0.2
var Corrosion_DROP_CHANCE : float = 0.2
var Cryo_DROP_CHANCE : float = 0.2


# res://characters/character.tscn
var Character_MAX_HP : int = 1000000000
var Character_MAX_SP : int = 120
var Character_INITIAL_SP : int = 40
var Character_SPEED : float = 4

# res://enemies/combat_operative.tscn
var CombatOperative_MAX_HP : int = 30
var CombatOperative_SPEED : float = 2.5
var CombatOperative_SP_DROP_AMOUNT : int = 2
var CombatOperative_HP_DROP_AMOUNT : int = 2
var CombatOperative_DAMAGE : int = 10

# res://enemies/suicide_drone.tscn
var SuicideDrone_MAX_HP : int = 20
var SuicideDrone_SPEED : float = 8
var SuicideDrone_SP_DROP_AMOUNT : int = 5
var SuicideDrone_HP_DROP_AMOUNT : int = 5
var SuicideDrone_DAMAGE : int = 18

# res://enemies/sniper_drone.tscn
var SniperDrone_MAX_HP : int = 20
var SniperDrone_SPEED : float = 3
var SniperDrone_ATTACK_RADIUS : float = 7.5
var SniperDrone_FLEE_RADIUS: float = 5
var SniperDrone_SP_DROP_AMOUNT : int = 15
var SniperDrone_HP_DROP_AMOUNT : int = 15
var SniperDrone_DAMAGE : int = 30

var WeaponDMG_MULT_INCREASE_PER_SECOND : float = 0.005

# res://skills/electro_sword.tscn
var ElectroSword_DAMAGE : int = 7
var ElectroSword_COOLDOWN : float = 0.1
#var ElectroSword_SP_COST : int = 10

# res://skills/chain.tscn
var Chain_DAMAGE : int = 15
var Chain_COOLDOWN : float = 0.2
var Chain_RANGE: float = 6

# res://skills/baton.tscn
var Baton_DAMAGE : int = 8
var Baton_COOLDOWN : float = 0.2
var Baton_RADIUS : float = 2

# res://skills/shotgun.tscn
var Shotgun_DAMAGE : int = 20
var Shotgun_MAX_RANGE : float = 2.4
var Shotgun_COOLDOWN : float = 0.4
var Shotgun_SPREAD : int = 72

# res://skills/rifle.tscn
var Rifle_BULLET_DAMAGE : int = 8
var Rifle_BULLET_SPEED : int = 30
var Rifle_COOLDOWN : float = 0.1

# res://skills/circle_slash.tscn
var CircleSlash_DAMAGE : int = 30
var CircleSlash_SP_COST : int = 40
var CircleSlash_COOLDOWN : float = 0.3
var CircleSlash_RADIUS : float = 3.0

# res://skills/nanobot_aura.tscn
var NanobotAura_DAMAGE_PER_SECOND : int = 10
var NanobotAura_HEAL_PER_SECOND : int = 10
var NanobotAura_PROC_INTERVAL : int = 1
var NanobotAura_DURATION : float = 5
var NanobotAura_RADIUS : int = 3
#var NanobotAura_SP_COST : int = 10

# res://statuses/status_cryo.tscn
var StatusCryo_DURATION : float = 5
var StatusCryo_SPEED_MULTIPLIER : float = 0.3
var StatusCryo_SHATTER_CHANCE : float = 0.1
var StatusCryo_SHATTER_DMG_MULTIPLIER: float = 20.0

# res://statuses/status_corroding.tscn
var StatusCorroding_DMG_MULTIPLIER: float = 0.5
var StatusCorroding_DURATION : float = 5
var StatusCorroding_PROC_INTERVAL : float = 1

var StatusBlazed_DURATION : float = 5
var StatusBlazed_PROC_INTERVAL : float = 1
var StatusBlazed_EXPLOTION_DMG : int = 5

# res://statuses/status_blazed.tscn


var Spawner_CONFIG = [["Wave Configuration"], ["Warning: Any changes made to this document may automatically affect the deployed live game. The game reloads this configuration whenever Play is pressed."], ["Each second, the spawning logic advances one row down, rolls against the spawn chance, and if the spawn happens, will spawn X groups of size Y-Z members of the given type."], ["", "Chance", "Groups", "Minimum", "Maximum", "Type", "Variant"], ["Round:", "0:00:30", "", "", "", "", "", "Level 1 "], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "1", "2", "sniper_drone", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "2", "3", "combat_operative", "green"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "combat_operative", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "3", "2", "2", "combat_operative", "gray"], [], ["Round:", "0:00:30", "", "", "", "", "", "Level 2"], ["Spawn:", "100.00%", "2", "1", "2", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "combat_operative", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "2", "3", "suicide_drone", "red"], ["Spawn:", "100.00%", "2", "1", "2", "combat_operative", "green"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "3", "2", "2", "combat_operative", "blue"], [], ["Round:", "0:00:30", "", "", "", "", "", "Level 3"], ["Spawn:", "100.00%", "2", "2", "2", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "3", "1", "1", "combat_operative", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "suicide_drone", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "3", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "suicide_drone", "red"], ["Spawn:", "100.00%", "1", "1", "2", "combat_operative", "gray"], ["Wait:", "0:00:02"], [], [], ["Round:", "0:00:15", "", "", "", "", "", "Level 4"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "1", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "1", "suicide_drone", "green "], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "1", "suicide_drone", "green "], [], [], ["Round:", "0:00:30", "", "", "", "", "", "Level 5"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "1", "2", "combat_operative", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "2", "2", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "2", "2", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "1", "2", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "2", "2", "combat_operative", "green"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "3", "2", "2", "combat_operative", "gray"], [], [], ["Round:", "0:00:30"], ["Spawn:", "100.00%", "2", "1", "2", "combat_operative", "gray", "Level 6"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "combat_operative", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "suicide_drone", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "2", "2", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "suicide_drone", "green"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "3", "2", "2", "combat_operative", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "3", "2", "2", "combat_operative", "gray"], [], [], ["Round:", "0:00:30", "", "", "", "", "", "Level 7"], ["Spawn:", "100.00%", "2", "3", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "1", "combat_operative", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "", "1", "1", "suicide_drone", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "3", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "3", "2", "2", "combat_operative", "gray"], ["Wait:", "0:00:02"], [], [], ["Round:", "0:00:15", "", "", "", "", "", "Level 8"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "suicide_drone", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "1", "suicide_drone", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "2", "suicide_drone", "gray"], ["Wait:", "0:00:02"], [], [], ["Round:", "0:00:30", "", "", "", "", "", "Level 9"], ["Spawn:", "100.00%", "2", "3", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "2", "1", "1", "combat_operative", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "4", "1", "1", "suicide_drone", "blue"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "3", "3", "combat_operative", "gray"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "1", "2", "3", "suicide_drone", "red"], ["Wait:", "0:00:02"], ["Spawn:", "100.00%", "5", "2", "2", "combat_operative", "gray"], ["Wait:", "0:00:02"]]

###############################################################################

signal updated()

func _ready():
	update()

func update():
	_update_static_vars()
	updated.emit()


func _update_static_vars():
	Enemy.update_weights()
	SkillDB.update_weights()
