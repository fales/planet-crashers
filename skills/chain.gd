extends Skill

@export var DAMAGE = 5
var dmg_multiplier = 1.0
var effective_damage = DAMAGE

var _anim_length = 1.0

@onready var sprite = $sprite


func _ready() -> void:
	sprite.hide()
	_anim_length = sprite.sprite_frames.get_frame_count("normal") / float(sprite.sprite_frames.get_animation_speed("normal"))
	$dmg_up.timeout.connect(func():
		dmg_multiplier += GameConfig.WeaponDMG_MULT_INCREASE_PER_SECOND
		effective_damage = DAMAGE * dmg_multiplier
		)
	super()


func _launch():
	sprite.show()
	sprite.play()
	if character.aim != Vector2.ZERO:
		scale.x = sign(character.aim.x)
	$hitbox.deal_damage(effective_damage, { "type" : "slash", "effect" : effect })
	await get_tree().create_timer(COOLDOWN).timeout
	sprite.hide()
	sprite.scale.y = -sprite.scale.y


func _update_params():
	DAMAGE = GameConfig.Chain_DAMAGE
	COOLDOWN = GameConfig.Chain_COOLDOWN
	sprite.speed_scale = _anim_length / COOLDOWN
	var range_in_pixels = GameConfig.Chain_RANGE * Globals.PIXELS_PER_UNIT
	($hitbox.shape as RectangleShape2D).size.x = range_in_pixels
	$hitbox.position = Vector2(16 + 0.5 * range_in_pixels,-3)
	sprite.scale = Vector2.ONE * (range_in_pixels / 192) # Divisor based on current asset dimensions


func _update_effect():
	sprite.animation = effect_name
