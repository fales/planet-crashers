extends Node2D

@export var spawn_parent_default : Node

signal tick_spawn()
signal next_wave()
signal all_spawns_finished()

var _round := 0
var _round_timer
var _round_timer_enabled := Globals.round_timer_enabled
var _wait_for_next_wave := true
var _alive := 0
var _wait_secs_between_rounds = 5
var _is_game_over := false

var _spawn_fx = preload("res://fx/spawn/spawn.tscn")
var _enemy_types = {
	"suicide_drone": preload("res://enemies/suicide_drone.tscn"),
	"combat_operative": preload("res://enemies/combat_operative.tscn"),
	"sniper_drone": preload("res://enemies/sniper_drone.tscn")
}

var _enemy_variants = {
	"red": Enemy.Types.RED,
	"green": Enemy.Types.GREEN,
	"blue": Enemy.Types.BLUE,
	"gray": Enemy.Types.GRAY,
	"grey": Enemy.Types.GRAY,
}

func _ready():
	if multiplayer.is_server():
		Events.game_begin.connect(start)
		Events.game_over.connect(_game_over)
		Events.enemy_killed.connect(_enemy_killed)

func _game_over(players : int, players_alive : int, round : int):
	_is_game_over = true

func _enemy_killed():
	_alive -= 1
	_alive = max(0, _alive)
	if _alive < 1:
		next_wave.emit()

func _spawn_group(type, variant : Enemy.Types, num=-1, offset_range=30, time_between=0.4):
	if num < 1:
		num = randi_range(2, 4)

	var screen_offset = offset_range
	var group_position = Vector2(
		randi_range(offset_range+screen_offset, Globals.LEVEL_SIZE.x-offset_range-screen_offset),
		randi_range(offset_range+screen_offset, Globals.LEVEL_SIZE.y-offset_range-screen_offset))

	for i in range(0, num):
		var e = type.instantiate()
		e.type = variant
		var spawn_position = Vector2(
			randi_range(group_position.x-offset_range, group_position.x+offset_range),
			randi_range(group_position.y-offset_range, group_position.y+offset_range))
		spawn(e, spawn_position, spawn_parent_default)
		await get_tree().create_timer(time_between).timeout

func start():
	_round = 0
	_alive = 0
	_is_game_over = false
	if not multiplayer.is_server():
		return
	var first_round_found := false
	for row in GameConfig.Spawner_CONFIG:
		if len(row) < 2:
			continue
		if not first_round_found:
			if row[0] != "Round:":
				continue
			first_round_found = true
		await _handle_row(row)
		await tick_spawn
		if _is_game_over:
			return
	if not _is_game_over:
		all_spawns_finished.emit()

func _parse_duration(value):
	return int(Time.get_unix_time_from_datetime_string(value))

func _handle_row(row):
	match row[0]:
		"Round:":
			var duration_seconds = _parse_duration(row[1])
			if duration_seconds < 1:
				print("Invalid round duration: ", row)
				return
			if _round_timer != null:
				if _round_timer.time_left > 0:
					await _round_timer.timeout
			if _round_timer_enabled:
				_round_timer = get_tree().create_timer(duration_seconds)
			if _wait_for_next_wave and _round > 0:
				await next_wave
			if _round > 0:
				_end_round.rpc(_round)
			if _wait_secs_between_rounds > 0 and _round > 0:
				await get_tree().create_timer(_wait_secs_between_rounds).timeout
			_round += 1
			_begin_round.rpc(_round, duration_seconds)
		"Wait:":
			var duration_seconds = _parse_duration(row[1])
			if duration_seconds < 1:
				print("Invalid wait duration: ", row)
				return
			await get_tree().create_timer(duration_seconds).timeout
		"Spawn:":
			if len(row) < 7:
				print("Invalid spawn value: ", row)
				return
			_handle_spawn_row(row, int(row[1]), int(row[2]), int(row[3]), \
							  int(row[4]), str(row[5]).strip_edges(), str(row[6]).strip_edges())
		_:
			print("Unknown spawn value: ", row)

@rpc("authority", "call_local", "reliable")
func _begin_round(round, duration_seconds):
	_alive = 0
	Events.round_begin.emit(round, duration_seconds)

@rpc("authority", "call_local", "reliable")
func _end_round(round):
	if _is_game_over:
		return
	Events.round_end.emit(round)

@warning_ignore("shadowed_global_identifier")
func _handle_spawn_row(row := [], chance := 100, groups := 1, min := 1, max := 1, t := "random", v := "random"):
	chance = clamp(chance, 1, 100)
	groups = clamp(groups, 1, 99)
	min = clamp(min, 1, 99)
	max = clamp(max, 1, 99)
	if chance != 100:
		if randi_range(1, 100) > chance:
			return
	if not t in _enemy_types and t != "random":
		print("Uknown enemy type: ", row)
		t = "random"
	var enemy = _enemy_types[_enemy_types.keys().pick_random()]
	if t != "random":
		enemy = _enemy_types[t]
	if not v in _enemy_variants and v != "random":
		print("Unknown enemy variant: ", row)
		v = "random"
	# Use weighted random
	var variant : Enemy.Types = Enemy.type_rng.next() as Enemy.Types
	if v != "random":
		variant = _enemy_variants[v]
	
	for group in range(0, groups):
		_spawn_group(enemy, variant, randi_range(min, max))

func tick():
	if _is_game_over:
		return
	tick_spawn.emit()

func get_round() -> int:
	return _round

func get_round_time_left_secs() -> float:
	if _round_timer == null:
		return 0.0
	return _round_timer.time_left

func spawn(spawn_scene, spawn_position, spawn_parent):
	if _is_game_over:
		return
	_alive += 1
	var _spawn = _spawn_fx.instantiate()
	spawn_scene.position = spawn_position
	spawn_scene.add_child(_spawn)
	spawn_parent.add_child.call_deferred(spawn_scene, !spawn_scene.name.is_valid_int())
