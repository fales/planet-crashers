extends GPUParticles2D

var parent : Node2D
@export_exp_easing("inout") var ease_curve = 0

func _draw():
	if parent and parent._active:
		var _circle_fill = ease(remap(parent.proc_timer.time_left, parent.PROC_INTERVAL, 0, 0, 1), ease_curve)
		draw_arc(Vector2.ZERO, parent.RADIUS + randi_range(1,2), 0, TAU, 64, parent._aura_color, 4)
		draw_circle(Vector2.ZERO, _circle_fill * parent.RADIUS, parent._aura_color)
