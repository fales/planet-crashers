extends Skill

@export var DAMAGE = 5

func _ready() -> void:
	$sprite.hide()


func _launch():
	$sprite.show()
	$sprite.play()
	rotation = character.aim.angle()
	$hitbox.deal_damage(DAMAGE)
