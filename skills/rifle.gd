extends Skill

@export var bullet_scene : PackedScene
@export var BULLET_DAMAGE := 5
@export var BULLET_SPEED := 1000
var dmg_multiplier = 1.0
var effective_damage = BULLET_DAMAGE

@onready var spawnpoint : Marker2D = $spawnpoint

@onready var sprite = $spawnpoint/sprite

func _ready() -> void:
	sprite.hide()
	$dmg_up.timeout.connect(func():
		dmg_multiplier += GameConfig.WeaponDMG_MULT_INCREASE_PER_SECOND
		effective_damage = BULLET_DAMAGE * dmg_multiplier
		)
	super()

func _launch():
	if character.aim.x != 0:
		spawnpoint.position.x = sign(character.aim.x) * abs(spawnpoint.position.x)
	
	var level = character.get_parent()
	var bullet = bullet_scene.instantiate()
	assert(bullet is Bullet)
	level.add_child(bullet)
	bullet.setup(spawnpoint.global_position, character.aim * BULLET_SPEED, effective_damage, { "type" : "electric", "effect" : effect })
	
	sprite.rotation = character.aim.angle()
	sprite.show()
	sprite.play()
	await sprite.animation_finished
	sprite.hide()

func _update_params():
	BULLET_DAMAGE = GameConfig.Rifle_BULLET_DAMAGE
	BULLET_SPEED = GameConfig.Rifle_BULLET_SPEED * Globals.PIXELS_PER_UNIT
	COOLDOWN = GameConfig.Rifle_COOLDOWN

func _update_effect():
	sprite.animation = effect_name
