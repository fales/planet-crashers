@tool
class_name HitboxPolygon
extends Polygon2D
## Allows to define shapecast-based hitboxes using arbitrary polygons (convex or concave).
##
## Currently unused in the prototype


## Sets the proper collision mask depending on what objects it should target
@export var is_enemy = true :
	set(value):
		is_enemy = value
		_update_shape_casts_collision()


var _shape_casts : Array[ShapeCast2D] = []

var _last_polygon = PackedVector2Array()


func _init() -> void:
	color = Color(0,0,0,0)
	is_enemy = is_enemy # Setter doesn't seem to trigger automatically as in Godot 3


func _enter_tree():
	draw.connect(_update_shape_casts)


func _set(property, value) -> bool:
	if property == "color":
		color = Color(0,0,0,0)
		return true
	return false


func _update_shape_casts_collision():
	for shape_cast in _shape_casts:
		# If enemy target players else target enemies
		shape_cast.collision_mask = Globals.PLAYER_COLLISION_LAYER if is_enemy else Globals.ENEMY_COLLISION_LAYER


## Called when the polygon is modified.
##
## Creates the necessary [ShapeCast2D] nodes to handle the polygon shape. If the polygon is concave
## it will be decomposed into several convex shapes
func _update_shape_casts():
	# Do not regenerate if the polygon is exactly the same as before
	if polygon == _last_polygon:
		return
	_last_polygon = polygon
	print("updating shape casts..")
	for node in _shape_casts:
		node.queue_free()
	
	_shape_casts.clear()
	
	# Decompose into convex shapes. Handles concave polygons
	var convex_hulls = Geometry2D.decompose_polygon_in_convex(polygon)
	
	# Create a ShapeCast2D for each convex shape
	for hull in convex_hulls:
		var convex_shape = ConvexPolygonShape2D.new()
		convex_shape.points = hull
		var shape_cast = ShapeCast2D.new()
		shape_cast.shape = convex_shape
		shape_cast.target_position = Vector2.ZERO
		shape_cast.collide_with_areas = true
		shape_cast.collide_with_bodies = false
		shape_cast.collision_mask = 2 if is_enemy else 4 #target players else enemies
		
		_shape_casts.append(shape_cast)
		add_child(shape_cast)


## Forces an update of all shapecasts and calls the damage method on the owner of any
## [Hurtbox] instances it hits
## [param amount]: amount of damage to cause
## [param custom_data]: A dictionary with any extra info about the damage. Currently used for 
## status effects and damage type
func deal_damage( amount : int, custom_data:= {} ) -> int:
	var targets = _get_target_list()
	for target in targets:
		target.damage(amount, custom_data)
	return targets.size()


# Returns an array of all the [Hurtbox] instances colliding with any of the shapecasts
func _get_target_list() -> Array:
	# Use a set, since we don't want duplicates
	var target_set := {}
	for shape in _shape_casts:
		shape.force_shapecast_update()
		var target_count = shape.get_collision_count()
		for i in target_count:
			var target = shape.get_collider(i).owner
			target_set [target] = true
	return target_set.keys()
