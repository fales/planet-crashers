class_name WeightedRandom
extends RefCounted
## Implements a weighted random choice
##
## Useful for drops or anything involving several possible outcomes with different chances
## Must be initialized with an array of weights representing the proportinal likeliness of each outcome
## [method next] will return an int from 0 to [code]weights.size()-1[/code] representing the chosen outcome

## An array of floats representing the proportional likeliness of each outcome
## For example, [code][4.0, 1.0, 2.0][/code] means option 0 is 4 times as likely as option 1 and
## twice as likely as option 2
var _weights : PackedFloat32Array

## Sum of all weights
var _total : float = 0.0


func _init( weights : PackedFloat32Array ) -> void:
	_weights = weights
	for weight in weights:
		assert( weight >= 0)
		_total += weight


## Returns an int representing the chosen outcome. Generally it will be use as an idex
## of an array holding the actual options e.g [code]var chosen_drop = drop_list[drop_rng.next()][/code]
func next() -> int:
	var remaining_distance = randf() * _total
	
	for i in _weights.size():
		remaining_distance -= _weights[i]
		if remaining_distance < 0:
			return i
	return -1


## Normalizes the weights and returns them in the range 0..100. Useful for displaying them to players
## in a more intuitive way
func get_weights_as_percentages() -> PackedFloat32Array:
	var ret := _weights.duplicate()
	# If all weights are 0 we don't need to do anything. Also avoid division by 0
	if _total == 0.0:
		return ret
	
	for i in ret.size():
		ret[i] = ret[i] / _total * 100
	return ret
	
