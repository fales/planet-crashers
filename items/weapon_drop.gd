extends Item
class_name WeaponDrop

const Types = SkillDB.Weapons
const Effects = SkillDB.Effects


@export var drop_type : Types = Types.SWORD :
	set(v):
		drop_type = v
		_update_animation()

@export var drop_effect : Effects = Effects.NORMAL :
	set(v):
		drop_effect = v
		_update_animation() 

@export var drop : String:
	set(v):
		drop = v
		_update_animation()

var _is_ready := false

func _ready() -> void:
	_is_ready = true
	super()
		
func set_random():
	if !_is_ready:
		await ready
	if is_multiplayer_authority():
		drop_type = SkillDB.weapon_rng.next() as Types
		drop_effect = SkillDB.effect_rng.next() as Effects


func _apply_effect(target : Area2D) -> void:
	if not target.owner is Character:
		return
	var player : Character = target.owner as Character
	player._drop_current_attack.rpc()
	player.equipped_attack = drop_type
	player.equipped_effect = drop_effect

func _update_animation():
	if !_is_ready:
		await ready
	$sprite.play(SkillDB.weapon_info[drop_type].name + "_" + SkillDB.effect_names[drop_effect])
