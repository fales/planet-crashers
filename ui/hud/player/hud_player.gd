extends Control

@export var announce : RichTextLabel
@export var announce_timer : RichTextLabel
@export var fade := true

var _tween
var _round_timer
var _is_game_over := false

func _ready():
	Events.game_over.connect(_game_over)
	if Globals.is_headless():
		return
	announce.text = ""
	announce_timer.text = ""
	modulate.a = 0
	Events.game_shown.connect(_fade_in)
	Events.game_hidden.connect(_fade_out)
	Events.round_begin.connect(_round_begin)
	Events.round_end.connect(_round_end)
	Events.game_begin.connect(_game_begin)

@warning_ignore("unused_parameter")
func _process(delta):
	if _round_timer == null or _is_game_over:
		return
	announce_timer.text = "[center]" + _time_convert(ceil(_round_timer.time_left)) + "[/center]"

func _fade_in():
	_is_game_over = false
	announce.text = ""
	announce_timer.text = ""
	announce_timer.modulate.a = 0
	modulate.a = 0
	@warning_ignore("shadowed_variable")
	var fade = get_tree().create_tween() 
	fade.tween_property(self, "modulate:a", 1, 1)

func _fade_out():
	@warning_ignore("shadowed_variable")
	var fade = get_tree().create_tween() 
	fade.tween_property(self, "modulate:a", 0, 1)

@warning_ignore("shadowed_global_identifier")
func _round_begin(round : int, duration_secs : int):
	print("Round begin: ", round)
	if not Globals.round_timer_enabled:
		await _announce("ROUND " + str(round))
		return
	if _round_timer != null and _round_timer.time_left > 0 and _round_timer.time_left < 3:
		await _round_timer.timeout
	@warning_ignore("shadowed_variable")
	var fade = get_tree().create_tween() 
	fade.tween_property(announce_timer, "modulate:a", 0, 0.5)
	await fade.finished
	await _announce("ROUND " + str(round))
	await _tween.finished
	_round_timer = get_tree().create_timer(duration_secs)
	fade = get_tree().create_tween() 
	fade.tween_property(announce_timer, "modulate:a", 1, 0.5)

func _round_end(round : int):
	print("Round end: ", round)
	_announce("ROUND END")


func _game_begin():
	_is_game_over = false


func _game_over(players : int, players_alive : int, round : int):
	_is_game_over = true
	if multiplayer.is_server():
		get_tree().create_timer(15).timeout.connect(_announce_restart_rpc)
		get_tree().create_timer(45).timeout.connect(_restart)
	if Globals.is_headless():
		return
	get_tree().create_timer(20).timeout.connect(_restart_popup)
	if players_alive > 0:
		_announce("GAME WON, YOU SURVIVED ALL " + str(round) + " WAVES", 0.1, false)
	else:
		_announce("GAME OVER, YOU MADE IT TO WAVE " + str(min(round, 1)), 0.1, false)
	@warning_ignore("shadowed_variable")
	var fade = get_tree().create_tween() 
	fade.tween_property(announce_timer, "modulate:a", 0, 0.5)

func _restart_popup():
	if Globals.is_headless():
		return
	var p = Globals.popup("Restart or quit to main menu?", "Restart?", "Restart", "Quit")
	p.canceled.connect(_back_to_main_menu)
	Events.game_restart.connect(p.queue_free)

func _back_to_main_menu():
	Events.game_end.emit()

func _announce_restart_rpc():
	_announce_restart.rpc()

@rpc("any_peer", "reliable", "call_local")
func _announce_restart():
	_announce("Game will restart in 30 seconds...", 0.1, false)

func _restart():
	_restart_rpc.rpc()

@rpc("any_peer", "reliable", "call_local")
func _restart_rpc():
	print("Restarting game")
	Events.game_restart.emit()

func _announce(text : String, speed := 0.2, should_fade = fade):
	var duration = speed * len(text)
	if _tween != null and _tween.is_valid() and _tween.is_running():
		await _tween.finished
	if len(announce.text) > 0:
		_tween = get_tree().create_tween()
		_tween.tween_property(announce, "visible_characters", 0, duration)
		await _tween.finished
	announce.visible_characters = 0
	announce.self_modulate.a = 1
	announce.text = "[center]" + text + "[/center]"
	_tween = get_tree().create_tween()
	_tween.tween_property(announce, "visible_characters", len(text), duration)
	if should_fade:
		_tween.tween_property(announce, "self_modulate:a", 0, 1).set_delay(1.0)

func _time_convert(time_in_sec : int) -> String:
	var seconds = time_in_sec%60
	@warning_ignore("integer_division")
	var minutes = (time_in_sec/60)%60
	return "%02d:%02d" % [minutes, seconds]
