@tool
class_name RetroText
extends Node2D
## Renders monospaced text based on a spritesheet and a charset

## Text string to display
@export var text = "":
	set(v):
		text = v
		queue_redraw()

## Spritesheet containing the glyphs to use
@export var font: Texture2D = preload("res://assets/sfx/ui/UI_Font.png"):
	set(v):
		font = v
		_recalculate_and_redraw()

## Width and height in pixels of each glyph in the font
@export var font_size : Vector2i = Vector2i(9,11):
	set(v):
		font_size = v
		_recalculate_and_redraw()

## Space between each letter when rendering text
@export var letter_spacing : int = 0:
	set(v):
		letter_spacing = v
		queue_redraw()

## String containing the list of charaters implemented by the spritesheet font
## They will be mapped left to right, top to bottom
@export var charset: String = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_'abcdefghijklmnopqrstuvwxyz{|}~»":
	set(v):
		if v.is_empty():
			v = " "
		charset = v
		_recalculate_and_redraw()

## What to use as a placeholder character for anything outside the charset
## If set to an invalid value the first character in the charset will be used
@export var replacement_char: String = " ":
	set(v):
		if v.is_empty() or not (v[0] in charset):
			v = charset[0]
		replacement_char = v[0]
		_recalculate_and_redraw()


var _frames_h := 16

# maps ascii characters to the region containing the corresponding glyph in the spritesheet
var _char_map : Dictionary = {}

func _recalculate_and_redraw():
	_frames_h = font.get_size().x / font_size.x
	_char_map.clear()
	for c in charset:
		_char_map[c] = _get_char_rect(c)
	queue_redraw()

func _draw() -> void:
	if charset.is_empty():
		return
	# For each character in the text to display, get the destination rect (where to render)
	# And the source rect (which glyph to render) and render it
	for i in text.length():
		draw_texture_rect_region(
			font,
			_get_dest_rect(i),
			# Not sure if this is faster than just doing _get_char_rect(text[i])
			_char_map.get(text[i], _char_map[replacement_char])
		)

# Returns the region of the spritesheet font containing the character passed as an argument
func _get_char_rect( character: String ):
	var char_id = charset.find(character)
	if char_id == -1:
		char_id = 0
	var src_pos = Vector2i(
			font_size.x * (char_id % _frames_h),
			font_size.y * (char_id/_frames_h))
	return Rect2(src_pos, font_size)

# Returns the destination rect for the passed offset, considering glyph size and letter spacing
func _get_dest_rect( offset : int):
	return Rect2(
		Vector2(offset * (font_size.x + letter_spacing), 0),
		font_size
		)
