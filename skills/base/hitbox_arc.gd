@tool
class_name HitboxArc
extends Hitbox
## A [Hitbox] with a circle sector as a collision shape
##
## Currently used by the shotgun skill


## Direction the sector points to, in degrees. Points right by default
@export_range(0,360) var direction : float = 0.0:
	set(v):
		direction = v
		_update_shape()


## Central angle of the sector, in degrees, i.e. how "wide" the collider is
@export_range(0,180) var angle : float = 0.0:
	set(v):
		angle = v
		_update_shape()


## Radius of the sector, i.e. how far the collider reaches
@warning_ignore("shadowed_global_identifier")
@export_range(0,1024) var range : float = 10.0:
	set(v):
		range = v
		_update_shape()


## Precision of the collider arc
##
## The actual collider is a convex polygon. Increase this value to increase the amount
## of vertices in the arc
@export_range(8,64) var precision : int = 16:
	set(v):
		precision = v
		_update_shape()

func _init() -> void:
	super()
	
func _enter_tree() -> void:
	_update_shape()


func _update_shape():
	# Calculate the start and end angles in radians
	var angle_from = deg_to_rad(direction - 0.5 * angle)
	var angle_to = deg_to_rad(direction + 0.5 * angle)

	# If the arc is a full circle the resulting shape is a [precision]-sided regular polygon
	# Adjust proportionally otherwise, with a minimum of 3 points
	var arc_points : int = max(precision * abs(angle_to - angle_from) / TAU, 3)
	
	# Build the polygon
	var points = PackedVector2Array()
	points.append(Vector2.ZERO)
	for i in arc_points:
		var _angle = lerp(angle_from, angle_to, float(i) / (arc_points-1) )
		points.append(Vector2.RIGHT.rotated(_angle) * range )
	
	# If the shape property is null create a new ConvexPolygonShape2D
	if not shape is ConvexPolygonShape2D:
		shape = ConvexPolygonShape2D.new()
	
	# Set the shape points using the created polygon
	if points != shape.points:
		shape.set_point_cloud(points)


# Make sure the shape property only accepts ConvexPolygonShape2D. Create an empty one otherwise
func _set(property: StringName, value: Variant) -> bool:
	if property == &"shape":
		if value is ConvexPolygonShape2D:
			return false
		else:
			shape = ConvexPolygonShape2D.new()
			update_configuration_warnings()
			return true
	return false
	

func _get_configuration_warnings() -> PackedStringArray:
	var ret = []
	if not shape or not shape is ConvexPolygonShape2D:
		ret = ["HitboxArc requiers a ConvexPolygonShape2D to work"]
	return ret
