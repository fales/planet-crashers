@tool
extends Sprite2D

signal blink_finished

enum Types { FLOOR, ARROW_LEFT, ARROW_RIGHT }

@export var type : Types = Types.FLOOR :
	set(val):
		type = val
		match val:
			Types.ARROW_LEFT:
				texture = preload("res://assets/sprites/warning_arrow.png")
				flip_h = true
			Types.ARROW_RIGHT:
				texture = preload("res://assets/sprites/warning_arrow.png")
				flip_h = false
			Types.FLOOR:
				texture = preload("res://assets/sprites/warning_floor.png")
				flip_h = false

func blink():
	$anim.play("blink")

func _on_anim_animation_finished(anim_name):
	blink_finished.emit()
