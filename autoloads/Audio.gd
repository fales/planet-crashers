extends Node

var default_max_polyphony = 5

## Set containing all the active sound effects
var _active_sfx := {}

# MUSIC

## Holds a reference to the AudioStreamPlayer currently playing music
var _current_music_track : AudioStreamPlayer

enum Bus { GAMEPLAY, MUSIC, MENU, UI }

const BusStr = {
	Bus.GAMEPLAY: &"Gameplay",
	Bus.MUSIC: &"Music",
	Bus.MENU: &"Menu Music",
	Bus.UI: &"UI"
}

## Returns the current music AudioStream
func music_get_stream() -> AudioStream :
	return _current_music_track.stream


## Returns the resource path of the current music AudioStream if available else ""
func music_get_stream_as_string() -> String :
	if _current_music_track.stream is AudioStream:
		return _current_music_track.stream.resource_path
	else:
		return ""


## Returns true if music is playing
func music_is_playing() -> bool:
	return is_instance_valid(_current_music_track) and _current_music_track.playing


## Plays the selected audio file, with optional crossfade
##
## - `path_or_audiostream`: A path (String) or an AudioStream to play.
## - `crossfade_duration`: Crossfade, in seconds, between the current music track and the next one
func music_play(path_or_audiostream, crossfade_duration := 0.5, audio_bus:= Bus.MUSIC):
	var new_track = _create_player(path_or_audiostream, BusStr[audio_bus])
	new_track.stream.loop = true
	if new_track.stream == _current_music_track and is_instance_valid(_current_music_track) and _current_music_track.stream_paused:
		_current_music_track.stream_paused = false
		return

	_stop_sound(_current_music_track, crossfade_duration)
	_play_sound(new_track, crossfade_duration)
	_current_music_track = new_track


## Pauses the current music track
func music_pause() -> void :
	_current_music_track.stream_paused = true

## Resumes the current music track
func music_unpause() -> void : 
	_current_music_track.stream_paused = false


## Stops the music
func music_stop(fade_duration := 0.5) -> void:
	_stop_sound(_current_music_track, fade_duration)
	_current_music_track = null

## SFX

## Play a sound effect with optional bus and props
@rpc("any_peer", "call_local", "reliable")
func sfx_play(stream_or_path, audio_bus := Bus.GAMEPLAY, props:= {}) -> AudioStreamPlayer:
	var sfx = _create_player(stream_or_path, BusStr[audio_bus], props)
	sfx.finished.connect(_stop_sound.bind(sfx))
	_play_sound(sfx, 0, true)
	return sfx


## Force stop a sound effect
func sfx_stop(sfx : AudioStreamPlayer) -> bool:
	if not _active_sfx.get(sfx):
		push_error("Stream player %s not handled by the audio singleton" % sfx)
		return false
	_stop_sound(sfx)
	return true


## Get the list of active sound effects
func get_active_sfx() -> Array:
	return _active_sfx.keys()


func _create_player(path_or_audiostream, audio_bus : String, props:= {}) -> AudioStreamPlayer:
	var ret := AudioStreamPlayer.new()
	ret.bus = audio_bus
	if path_or_audiostream is AudioStream:
		ret.stream = path_or_audiostream
	elif typeof(path_or_audiostream) == TYPE_STRING:
		var stream = load(path_or_audiostream)
		if stream is AudioStream:
			ret.stream = stream
		else:
			push_warning("Audio singleton could not load %s" % path_or_audiostream)
	else:
		push_error("'path_or_audiostream' should be a path string or an AudioStream")

	for prop in props.keys():
		ret.set(prop, props[prop])
	if not props.has("max_polyphony"):
		ret.max_polyphony = default_max_polyphony
	return ret


func _play_sound(track : AudioStreamPlayer, fade_duration := 0.0, add_to_list := false):
	if !track.stream:
		return
	# Used by sfx_play
	if add_to_list:
		_active_sfx[track] = true

	add_child(track)
	track.play()
	if fade_duration > 0.0:
		track.volume_db = -60
		var tweener = get_tree().create_tween()
		tweener.tween_property(track, "volume_db", 0.0, fade_duration)


func _stop_sound(track : AudioStreamPlayer, fade_duration := 0.0):
	if !is_instance_valid(track):
		return
	_active_sfx.erase(track)
	if fade_duration > 0.0:
		var tweener = get_tree().create_tween()
		tweener.tween_property(track, "volume_db", -60.0, fade_duration )
		tweener.tween_callback(track.queue_free)
	else:
		track.queue_free()
	
