extends "res://ui/label_fx/label_fx.gd"

func _ready() -> void:
	super()
	visibility_changed.connect(func(): get_v_scroll_bar().value = 0)

func _process(delta: float) -> void:
	get_v_scroll_bar().value = wrap( get_v_scroll_bar().value + 50 * delta, 0, get_v_scroll_bar().max_value - get_v_scroll_bar().get_page())
