image: ubuntu:focal

stages:
  - test
  - export
  - deploy

# If you change the below or the Godot build, you may want to clear CI caches:
# https://gitlab.com/<user>/<project>/-/pipelines -> "Clear runner caches".
# (Should be done from time to time anyway; caches aren't cleared automatically.)
variables:
  EXPORT_NAME: planet-crashers
  GODOT_VERSION: "4.2"
  GODOT_BRANCH: "stable"
  GODOT_BUILD: ${GODOT_VERSION}-${GODOT_BRANCH}
  GODOT_TEMPLATE_BUILD: ${GODOT_VERSION}.${GODOT_BRANCH}
  GODOT_DOWNLOAD_ENGINE: https://github.com/godotengine/godot-builds/releases/download/${GODOT_BUILD}/Godot_v${GODOT_BUILD}_linux.x86_64.zip
  GODOT_DOWNLOAD_TEMPLATES: https://github.com/godotengine/godot-builds/releases/download/${GODOT_BUILD}/Godot_v${GODOT_BUILD}_export_templates.tpz
  CLI_DOWNLOAD: wget --timestamping --tries=2 --timeout=30 --no-verbose
  CACHE_SUFFIX: "-0"

.default_rules:
  rules:
    # If $W4_ENABLE_CI isn't set, skip CI; likewise if $W4_ENABLE_CI exists and is set to 0.
    - if: $W4_ENABLE_CI == null || $W4_ENABLE_CI == "0"
      when: never
    # Skip CI if the commit title (first line of commit message) contains "skipci".
    - if: $CI_COMMIT_TITLE =~ /skipci/
      when: never
    # Skip CI if the commit title (first line of commit message) contains "draft".
    - if: $CI_COMMIT_TITLE =~ /draft/
      when: never
    # Skip CI if Gitlab deploy freeze feature is enabled.
    - if: $CI_DEPLOY_FREEZE != null
      when: never
    # Run if this is a tag.
    - if: $CI_COMMIT_TAG
    # Run if the branch starts with 'ci/' (for CI development purposes).
    - if: $CI_COMMIT_REF_NAME =~ /^ci\//
    # Run on commits to the default branch.
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

include:
  - template: Security/Secret-Detection.gitlab-ci.yml
    rules:
      # If $W4_ENABLE_CI isn't set to 1, skip CI.
      - if: $W4_ENABLE_CI == "1"

.setup_godot:
  rules:
    - !reference [.default_rules, rules]
  before_script:
    # Setup environment.
    - export DEBIAN_FRONTEND=noninteractive
    - apt-get update && apt-get install -y --no-install-recommends
      ca-certificates
      unzip
      wget
      zip
      build-essential
      libxcursor-dev
      libxinerama-dev
      libxrandr-dev
      libxi-dev
      libgl-dev
      libdbus-1-dev
      libfontconfig-dev
      git
      openssh-client

    # Workaround for https://github.com/godotengine/godot/issues/53163.
    - cd overrides && cp -r -v ./ ../ && cd ..

    # Now checkout the submodule
    - git submodule update --init

    # Insert Git commit hash
    - sed -i "s|{%CI_COMMIT_SHORT_SHA}|${CI_COMMIT_SHA}|g" autoloads/Globals.gd

    # Download Godot Engine if not downloaded yet, unzip, add to path.
    - >
      if [ ! -f "Godot_v${GODOT_BUILD}_linux.x86_64.zip" ]; then
        $CLI_DOWNLOAD $GODOT_DOWNLOAD_ENGINE
      fi
    - unzip -d /usr/local/bin/godot Godot_v${GODOT_BUILD}_linux.x86_64.zip
    - mv /usr/local/bin/godot/Godot_v${GODOT_BUILD}_linux.x86_64 /usr/local/bin/godot/godot
    - chmod +x /usr/local/bin/godot/godot
    - export PATH=/usr/local/bin/godot/:$PATH
    - mkdir -p .godot/editor/
    - mkdir -p .godot/imported/

    # Download Godot Engine templates, create template dir and move them there.
    - >
      if [ ! -f "Godot_v${GODOT_BUILD}_export_templates.tpz" ]; then
        $CLI_DOWNLOAD $GODOT_DOWNLOAD_TEMPLATES
      fi
    - unzip Godot_v${GODOT_BUILD}_export_templates.tpz
    - mkdir -p ~/.local/share/godot/export_templates/${GODOT_TEMPLATE_BUILD}/
    - mv templates/* ~/.local/share/godot/export_templates/${GODOT_TEMPLATE_BUILD}
    - godot --version --headless
  cache:
    - key: godot-${GODOT_BUILD}${CACHE_SUFFIX}
      paths:
        - Godot_v${GODOT_BUILD}_linux.x86_64.zip
        - Godot_v${GODOT_BUILD}_export_templates.tpz

export:linux:
  extends: .setup_godot
  stage: export
  script:
    - mkdir -v -p build/linux
    - cp -v misc/.itch-linux.toml build/linux/.itch.toml
    # We need to run Godot export twice as a workaround for https://github.com/godotengine/godot/issues/75388
    - godot -v --headless --export-debug "Linux/X11" build/linux/${EXPORT_NAME}.x86_64
    - godot -v --headless --export-debug "Linux/X11" build/linux/${EXPORT_NAME}.x86_64
    # Check if exported binary and .pck exist and if they are at least 10mb.
    - >
      if [[ ! $(find "build/linux/${EXPORT_NAME}.x86_64" -type f -size +10M 2>/dev/null) ]]; then
        echo "Export failed (Linux executable < 10mb)"
        exit 1
      fi
    - >
      if [[ ! $(find "build/linux/${EXPORT_NAME}.pck" -type f -size +1M 2>/dev/null) ]]; then
        echo "Export failed (Linux .pck < 1mb)"
        exit 1
      fi
    - chmod +x "build/linux/${EXPORT_NAME}.x86_64"
  artifacts:
    name: ${EXPORT_NAME}-${CI_JOB_NAME}
    expire_in: 5 days
    paths:
      - build/linux

export:windows:
  extends: .setup_godot
  stage: export
  script:
    - mkdir -v -p build/windows
    - cp -v misc/.itch-windows.toml build/windows/.itch.toml
    # We need to run Godot export twice as a workaround for https://github.com/godotengine/godot/issues/75388
    - godot -v --headless --export-debug "Windows Desktop" build/windows/${EXPORT_NAME}.exe
    - godot -v --headless --export-debug "Windows Desktop" build/windows/${EXPORT_NAME}.exe
    # Check if exported binary and .pck exist and if they are at least 10mb.
    - >
      if [[ ! $(find "build/windows/${EXPORT_NAME}.exe" -type f -size +10M 2>/dev/null) ]]; then
        echo "Export failed (Windows executable < 10mb)"
        exit 1
      fi
    - >
      if [[ ! $(find "build/windows/${EXPORT_NAME}.pck" -type f -size +1M 2>/dev/null) ]]; then
        echo "Export failed (Winodws .pck < 1mb)"
        exit 1
      fi
  artifacts:
    name: ${EXPORT_NAME}-${CI_JOB_NAME}
    expire_in: 5 days
    paths:
      - build/windows

# Itch.io deployment. Requires the following Gitlab secrets to be configured:
# - BUTLER_API_KEY: Itch.io API key.
# - ITCHIO_USERNAME: Itch.io username.
# If those are not configured, Itch.io deployment jobs aren't run.

# Setup / download Itch.io API client (butler), cached on Gitlab.
# See: https://itch.io/docs/butler/
.setup_butler:
  variables:
    ITCHIO_DOWNLOAD_BUTLER: https://broth.itch.ovh/butler/linux-amd64/LATEST/archive/default
  before_script:
    # Setup environment.
    - export DEBIAN_FRONTEND=noninteractive
    - apt-get update && apt-get install -y --no-install-recommends
      ca-certificates
      unzip
      wget
      zip
      python3

    # Download Itch.io butler, unzip, make executable and add to path.
    # Timestamps are used to check whether we need to download an update.
    # TODO: 'default' is a bad archive name, but renaming breaks timestamping.
    - $CLI_DOWNLOAD $ITCHIO_DOWNLOAD_BUTLER
    - mkdir -p /opt/butler/
    - unzip -d /opt/butler/ default
    - chmod +x /opt/butler/butler
    - export PATH=/opt/butler/:$PATH
    - butler --version
  cache:
    - key: butler
      paths:
        - default

.itch_deploy:
  stage: deploy
  extends: .setup_butler
  variables:
    RELEASE_CHANNEL: test
    RELEASE_VERSION: ${CI_COMMIT_TAG}
  script:
    - export ITCHIO_GAME=$EXPORT_NAME
    - >
      if test -z "${RELEASE_VERSION}"; then
        # The short Git commit hash is used as version number if one is not provided.
        export RELEASE_VERSION=${CI_COMMIT_SHORT_SHA}
      else
        # The "-channel" suffix is removed from the version number if present.
        export RELEASE_VERSION=$(echo "${RELEASE_VERSION}" | sed -e "s/-${RELEASE_CHANNEL}$//")
      fi
    - echo "Deploying to channel ${RELEASE_CHANNEL}, version ${RELEASE_VERSION}"
    - >
      for PLATFORM in linux windows; do
        butler validate --platform ${PLATFORM} --arch amd64 build/${PLATFORM}
        butler push build/${PLATFORM} ${ITCHIO_USERNAME}/${ITCHIO_GAME}:${RELEASE_CHANNEL}-${PLATFORM} --userversion ${RELEASE_VERSION}
      done
  dependencies:
    - export:linux
    - export:windows

itch:deploy-test:
  extends: .itch_deploy
  rules:
    # Only run if Itch is configured.
    - if: $ITCHIO_USERNAME == null || $BUTLER_API_KEY == null
      when: never
    # Run on default branch (main) and well known tags.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG =~ '/.*-test/'
  variables:
    RELEASE_CHANNEL: test

itch:deploy-staging:
  extends: .itch_deploy
  rules:
    - if: $ITCHIO_USERNAME == null || $BUTLER_API_KEY == null
      when: never
    - if: $CI_COMMIT_TAG =~ '/.*-staging/'
  variables:
    RELEASE_CHANNEL: staging

itch:deploy-release:
  extends: .itch_deploy
  rules:
    - if: $ITCHIO_USERNAME == null || $BUTLER_API_KEY == null
      when: never
    - if: $CI_COMMIT_TAG =~ '/.*-release/'
  variables:
    RELEASE_CHANNEL: release

