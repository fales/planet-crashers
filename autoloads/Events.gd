extends Node

signal round_begin(round : int, duration_seconds : int)
signal round_end(round : int)

signal enemy_killed()

signal game_shown()
signal game_hidden()

signal game_begin()
signal game_over(players : int, players_alive : int, round : int)
signal game_end()
signal game_restart()

signal game_error(msg : String)
