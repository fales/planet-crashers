extends Skill

@export var DAMAGE_PER_SECOND = 10
@export var HEAL_PER_SECOND = 10
@export var PROC_INTERVAL = 1
@export var DURATION = 5
@export var RADIUS = 160

@onready var enemy_detector : HitboxArea = $EnemyDetector
@onready var ally_detector : HitboxArea = $AllyDetector
@onready var particles : GPUParticles2D = $particles
@onready var aura : AnimatedSprite2D = $aura
@onready var proc_timer : Timer = $ProcTimer

var _active = false

var _anim_name = "aura_blue"
var _aura_color = Color(0,0,1,0.2)

func _launch():
	_active = true
	queue_redraw()
	particles.emitting = true
	aura.play(_anim_name)
	proc_timer.start(PROC_INTERVAL)
	set_process(true)
	
	$Timer.start(DURATION)
	await $Timer.timeout
	
	_active = false
	particles.emitting = false
	aura.play("none")
	proc_timer.stop()
	set_process(false)
	particles.queue_redraw()
	

func _ready():
	super()
	particles.emitting = false
	particles.parent = self
	aura.play("none")


func _on_proc_timer_timeout():
	enemy_detector.deal_damage(DAMAGE_PER_SECOND, {"type" : "electric"})
	ally_detector.deal_damage(-HEAL_PER_SECOND)


func _process(_delta):
	aura.flip_h = character.visuals.scale.x < 0
	particles.queue_redraw()

func _update_params():
	RADIUS = GameConfig.NanobotAura_RADIUS * Globals.PIXELS_PER_UNIT
	DAMAGE_PER_SECOND = GameConfig.NanobotAura_DAMAGE_PER_SECOND
	HEAL_PER_SECOND = GameConfig.NanobotAura_HEAL_PER_SECOND
	PROC_INTERVAL = GameConfig.NanobotAura_PROC_INTERVAL
	DURATION = GameConfig.NanobotAura_DURATION
	
	particles.process_material.emission_sphere_radius = RADIUS
	enemy_detector.get_node("CollisionShape2D").shape.radius = RADIUS


func _update_color():
	_anim_name = "aura" + color_suffix
	match color_suffix:
		"_blue":
			_aura_color = Color("a6fcdb")
		"_green":
			_aura_color = Color("d6f264")
		"_yellow":
			_aura_color = Color("fffc40")
	
	particles.process_material.color = _aura_color
	_aura_color.s = 1.0
	_aura_color.a = 0.4
	
