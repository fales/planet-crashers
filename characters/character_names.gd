extends RefCounted

# Currently max. 8 chars supported by UI
var names = [
	"Robi",
	"Cubio",
	"Gobot",
	"Godette",
	"Sophia",
	#"VariantCall",
	"mesege",
	#"Bobby Tables",
	#"TransientP",
	"Jetpaca",
	"Larvotor",
	"Tiwagos",
	"Phoenix",
	"W4GD",
	"Beckett",
	"Root",
	"Node",
	"Signal",
	"Peer",
	#"Authority",
	"Mergebot",
	#"Stroopwafel",
	"icon.png",
	"icon.svg"
]
