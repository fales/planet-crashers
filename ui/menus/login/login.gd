extends VBoxContainer

signal back_to_main_menu

const MIN_LEN = 5

func _ready():
	$form/buttons/quit.pressed.connect(get_tree().quit) 
	$form/user.text_submitted.connect(_choose_next)
	$form/pass.text_submitted.connect(_choose_next)
	$form/buttons/login.pressed.connect(_choose_next)
	$form/remember.toggled.connect(_remember_toggled)

	$form/user.text = PlayerConfig.user_email
	$form/pass.text = PlayerConfig.user_pass
	$form/remember.set_pressed_no_signal(PlayerConfig.user_store)

	_set_status()
	_choose_next()

func _remember_toggled(checked):
	if checked == PlayerConfig.user_store:
		return
	PlayerConfig.user_store = checked
	if not checked:
		PlayerConfig.user_pass = ""
	PlayerConfig.save()

@warning_ignore("unused_parameter")
func _choose_next(v = null):
	if len($form/user.text) < 1:
		$form/user.grab_focus()
		return

	if len($form/pass.text) < 1:
		$form/pass.grab_focus()
		return

	$form/buttons/login.grab_focus()
	_login()

func _back_pressed():
	back_to_main_menu.emit()

func _set_status(msg="", success=false):
	print("Login status: ", msg)
	var status = $form/status
	status.text = msg
	if not success:
		status.set("theme_override_colors/font_color", Color.RED)
	else:
		status.set("theme_override_colors/font_color", Color.LAWN_GREEN)

func _login() -> Error:
	var user = $form/user.text.strip_edges()
	var key = $form/pass.text.strip_edges()

	if len(user) < MIN_LEN:
		_set_status("eMail too short")
		return ERR_INVALID_PARAMETER

	if not user.contains("@"):
		_set_status("eMail invalid")
		return ERR_INVALID_PARAMETER

	if len(key) < MIN_LEN:
		_set_status("Password too short")
		return ERR_INVALID_PARAMETER

	var result = await W4GD.auth.signup_email(user, key).async()
	if not result:
		_set_status("Invalid server response")
		return ERR_BUG
	if result.is_error():
		if not str(result.message.as_string()).contains("already registered"):
			_set_status(result.message.as_string())
			return ERR_CANT_CONNECT

	result = await W4GD.auth.login_email(user, key).async()

	if not result:
		_set_status("Invalid server response")
		return ERR_BUG
	if result.is_error():
		var errMsg = str(result.message.as_string())
		_set_status(errMsg)
		if errMsg == "invalid_grant":
			return ERR_UNAUTHORIZED
		elif errMsg.find("try logging in again") > 0:
			print("Retrying")
			result = await W4GD.auth.login_device_auto().async()
			print("Retry: ", result.error.as_string())
		return ERR_CANT_CONNECT

	if not W4GD.get_identity().is_authenticated():
		_set_status("Not authenticated")
		return ERR_UNAUTHORIZED

	PlayerConfig.user_email = user
	if PlayerConfig.user_store:
		PlayerConfig.user_pass = key
	else:
		PlayerConfig.user_pass = ""
	PlayerConfig.save()

	_set_status("Login successful", true)
	get_tree().create_timer(1.0).timeout.connect(_back_pressed)
	return OK
