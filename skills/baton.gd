extends Skill

@export var DAMAGE = 5
var dmg_multiplier = 1.0
var effective_damage = DAMAGE

var _anim_length = 1.0

@onready var sprite = $sprite

func _ready() -> void:
	sprite.hide()
	_anim_length = sprite.sprite_frames.get_frame_count("normal") / float(sprite.sprite_frames.get_animation_speed("normal"))
	$dmg_up.timeout.connect(func():
		dmg_multiplier += GameConfig.WeaponDMG_MULT_INCREASE_PER_SECOND
		effective_damage = DAMAGE * dmg_multiplier
		)
	super()


func _launch():
	sprite.show()
	sprite.play()
	rotation = character.aim.angle()
	$hitbox.deal_damage(effective_damage, { "type" : "slash", "effect" : effect })
	await get_tree().create_timer(COOLDOWN).timeout
	sprite.hide()


func _update_params():
	DAMAGE = GameConfig.Baton_DAMAGE
	COOLDOWN = GameConfig.Baton_COOLDOWN
	sprite.speed_scale = _anim_length / COOLDOWN
	
	var radius_in_pixels = GameConfig.Baton_RADIUS * Globals.PIXELS_PER_UNIT
	($hitbox.shape as CircleShape2D).radius = radius_in_pixels
	sprite.scale = Vector2(1.0, 0.85) * ( radius_in_pixels / 51.2) # Based on base radius for current asset
	

func _update_effect():
	sprite.animation = effect_name
