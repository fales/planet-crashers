extends Control

var update_after_frames = 60
var show_details = false

func set_status(msg:=""):
	if show_details:
		$lower_right/version.text = msg + " | " + _get_cloud_url() + " | " + Globals.get_version()  + " | F3: Debug"
	else:
		$lower_right/version.text = Globals.get_version() + " | F3: Debug"

func _get_cloud_url():
	return W4GD.W4ProjectSettings.get_config().url.replace("https://", "").replace("/", "")

func _ready():
	$lower_right/margin/mute.toggled.connect(_mute_toggled)
	$lower_right/margin/mute.custom_minimum_size = Vector2(Globals.PIXELS_PER_UNIT, Globals.PIXELS_PER_UNIT)
	_update_mute_icon(PlayerConfig.audio_mute)
	_update_info()

func _mute_toggled(pressed):	
	$lower_right/margin/mute/click_sfx.play()
	_update_mute_icon(pressed)
	PlayerConfig.audio_mute = pressed
	PlayerConfig.save()

func _update_mute_icon(muted):
	$lower_right/margin/icon_off.visible = muted
	$lower_right/margin/icon_on.visible = !muted
	$lower_right/margin/mute.set_pressed_no_signal(muted)

@warning_ignore("unused_parameter")
func _process(delta):
	if Engine.get_process_frames() % update_after_frames == 0 && is_visible_in_tree():
		_update_info()

func _unhandled_input(event):
	if event.is_action_pressed("toggle_debug_info"):
		show_details = !show_details
		_update_info()
		## BEGIN UGLY
		var level_group = get_tree().get_nodes_in_group("level")
		while level_group.is_empty():
			await get_tree().create_timer(0.5).timeout
			level_group = get_tree().get_nodes_in_group("level")
		level_group[0].grid_enabled = show_details
		# END UGLY

func _update_info():
	$info.text =  ""
	if show_details:
		$info.text += "FPS: " + str(Performance.get_monitor(Performance.TIME_FPS)) + "\n"
		$info.text += "GPU Mem: " + _format_bytes(Performance.get_monitor(Performance.RENDER_VIDEO_MEM_USED)) + "\n"
		$info.text += "Objects: " + str(Performance.get_monitor(Performance.OBJECT_COUNT)) + "\n"
		$info.text += "└─ Nodes: " + str(Performance.get_monitor(Performance.OBJECT_NODE_COUNT)) + "\n"
		$info.text += "└─ Resources: " + str(Performance.get_monitor(Performance.OBJECT_RESOURCE_COUNT)) + "\n"
		$info.text += "└─ Physics: " + str(Performance.get_monitor(Performance.PHYSICS_2D_ACTIVE_OBJECTS)) + "\n"
		$info.text += "└─ Orphans: " + str(Performance.get_monitor(Performance.OBJECT_ORPHAN_NODE_COUNT)) + "\n"
		$info.text += "\nPress ESC to quit"

	var status = "Not logged in"
	if Net.is_logged_in():
		status = "Logged in: " + Net.get_id()
	set_status(status)

@warning_ignore("shadowed_variable_base_class")
func _format_bytes(size, decimals=2, step=1024):
	var units = ["B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB"]
	var largest_unit = "YB"

	for unit in units:
		if size < step:
			return ("%." + str(decimals) + "f %s") % [size, unit]
		size /= step

	return ("%." + str(decimals) + "f %s") % [size, largest_unit]
