extends Node2D
class_name Enemy


# STATUS EFFECTS
enum Statuses { NORMAL, BLAZED, CRYONIZED, CORRODING }

# TYPES
enum Types { GRAY, RED, GREEN, BLUE }

const type_stats := {
	Types.GRAY : {
			"weak" : [],
			"resist" : [],
		},
	Types.RED : {
			"weak" : [SkillDB.Effects.CRYO],
			"resist" : [SkillDB.Effects.BLAZING],
		},
	Types.GREEN : {
			"weak" : [SkillDB.Effects.NORMAL],
			"resist" : [SkillDB.Effects.CORROSION],
		},
	Types.BLUE : {
		"weak" : [SkillDB.Effects.BLAZING],
		"resist" : [SkillDB.Effects.CRYO],
		},
}


# DROPS
enum DropTypes { NOTHING, POWERCELL, STIMPACK, WEAPON }

const DROP_SCENES = {
	DropTypes.POWERCELL : preload("res://items/power_cell.tscn"),
	DropTypes.STIMPACK: preload("res://items/stim_pack.tscn"),
	DropTypes.WEAPON: preload("res://items/weapon_drop.tscn"),
}

static var drop_rng : WeightedRandom = WeightedRandom.new([0.8, 0.2, 0.15, 0.05])
static var type_rng : WeightedRandom = WeightedRandom.new([0.5, 0.2, 0.1, 0.2])

# STATS
## Maximum hitpoints
@export var MAX_HP : int = 100

## Speed in pixels per second
@export var SPEED : float = 1 * Globals.PIXELS_PER_UNIT

## Range at which the enemy should start attacking
@export var ATTACK_RANGE : float = 0

## How much sp it drops on death
@export var SP_DROP_AMOUNT : int = 1

## How much hp it drops on death
@export var HP_DROP_AMOUNT : int = 1

@export var HEALTHBAR_VISIBLE : bool = false

## Direction the enemy is moving. Exported only for sync purposes
@export var direction : Vector2 = Vector2.ZERO

## Direction the enemy intends to aim. Exported only for sync purposes
@export var target_aim : Vector2 = Vector2.RIGHT


@export var type : Types = Types.GRAY:
	set = _set_type

@export var status : Statuses = Statuses.NORMAL:
	set(v):
		status = v
		if !_is_ready:
			await ready
		$StatusEffect.type = v

var status_node : Node2D

## Direction the character is aiming. Always of magnitude 1. Use for directed attacks. 
var aim : Vector2 = target_aim

## Current hitpoints
var hp : int = MAX_HP:
	set(value):
		hp = clamp(value, 0, MAX_HP)
		if !_is_ready:
			await ready
		hp_bar.value = hp

var dead := false

@onready var hp_bar : PointBar = $hp_bar

const slash_fx_scene = preload("res://fx/slash_fx.tscn")
const shock_fx_scene = preload("res://fx/shock_fx.tscn")

var _is_ready := false

func _ready() -> void:
#	if is_multiplayer_authority():
#		type = type_rng.next() as Types
	_is_ready = true
	_update_params()
	GameConfig.updated.connect(_update_params)
	Events.game_over.connect(_game_over)
	

	if $visuals.get_child_count() == 0:
		_add_debug_sprite()


func _game_over(players : int, players_alive : int, round : int):
	if players_alive > 0:
		queue_free()


func _add_debug_sprite():
	var image = Image.create(32,32,false,Image.FORMAT_RGBA8)
	for i in 32:
		image.set_pixel(i, 0, Color.MAGENTA)
		image.set_pixel(i, 31, Color.MAGENTA)
		image.set_pixel(0, i, Color.MAGENTA)
		image.set_pixel(31, i, Color.MAGENTA)
		image.set_pixel(i, i, Color.MAGENTA)
	var debug_sprite = Sprite2D.new()
	var debug_texture = ImageTexture.create_from_image(image)
	debug_sprite.texture = debug_texture
	$visuals.add_child(debug_sprite)


func _update_aim():
	if target_aim == Vector2.ZERO:
		return
	aim = target_aim



## Called by anything wanting to hurt the character
## Negative amounts heal. This is how FF games handle it so you can turn any attack into a heal but it could be changed 
## to modify_hp or something like that so it's more descriptive
func damage(amount : int, custom_data:= {} ):
	_take_damage.rpc(amount, custom_data)


@rpc("any_peer", "call_local")
func _take_damage(amount : int, custom_data:= {}  ):
	if hp <= 0:
		return
	
	var effective_damage = amount

	# First pass damage info to status effect and see if they want to modify it
	if status_node:
		effective_damage = status_node.process_damage(amount, custom_data)

	# Second, handle weakesses and resistances
	var damage_effect = custom_data.get("effect", "")
	if damage_effect in type_stats[type].weak:
		effective_damage = amount * 1.25
		#printt("Weak to ", SkillDB.effect_names[damage_effect])
	elif damage_effect in type_stats[type].resist:
		effective_damage = amount * 0.5
		#printt("Resistant to ", SkillDB.effect_names[damage_effect])
	
	# Third apply status effects
	if status == Statuses.NORMAL:
		var effect : Node2D
		match damage_effect:
			SkillDB.Effects.BLAZING:
				status = Statuses.BLAZED
				effect = preload("res://statuses/status_blazed.tscn").instantiate()
			SkillDB.Effects.CRYO:
				status = Statuses.CRYONIZED
				effect = preload("res://statuses/status_cryo.tscn").instantiate()
			SkillDB.Effects.CORROSION:
				status = Statuses.CORRODING
				effect = preload("res://statuses/status_corroding.tscn").instantiate()
				effect.base_damage = amount
		if effect:
			add_child(effect)
			effect.position = $visuals.position
			effect.parent = self
			effect.start()
			effect.finished.connect(func (): status = Statuses.NORMAL; status_node = null )
			status_node = effect

	# Lastly, apply damage
	hp = clamp(hp - effective_damage, 0, MAX_HP)

	var modulate_color = Color.RED if amount > 0 else Color.GREEN
	var _damage_number_size = clamp(remap(effective_damage, 5, 300, 9, 18), 9, 18)
	var _damage_numbers = DamageNumber.new(
		get_parent(),
		position + Vector2(randf_range(-0.3, 0.3) * Globals.PIXELS_PER_UNIT,-Globals.PIXELS_PER_UNIT),
		abs(effective_damage),
		modulate_color,
		_damage_number_size)
	
	match custom_data.get("type", ""):
		"slash":
			var slash_fx = slash_fx_scene.instantiate()
			$visuals/sprite.add_child(slash_fx)
			slash_fx.play_fx()
		"electric":
			var shock_fx = shock_fx_scene.instantiate()
			$visuals/sprite.add_child(shock_fx)
			shock_fx.play()


	$visuals.modulate = Color(3,0,0) if amount > 0 else Color(0,3,0)
	await get_tree().create_timer(0.1).timeout
	$visuals.modulate = Color.WHITE

	if hp <= 0:
		die()



func die():
	dead = true
	_drop_item()
	queue_free()


func _exit_tree():
	Events.enemy_killed.emit()


func _drop_item():
	if not is_multiplayer_authority():
		return
	
	var chosen = drop_rng.next()
	if chosen == DropTypes.NOTHING:
		return

	Audio.sfx_play.rpc("res://assets/sfx/item/drop.wav")
	
	var drop : Node2D = (DROP_SCENES[chosen] as PackedScene).instantiate()
	drop.position = position.clamp(
			Vector2(Globals.LEVEL_PLAY_AREA.x, Globals.LEVEL_PLAY_AREA.y),
			Vector2(Globals.LEVEL_PLAY_AREA.z, Globals.LEVEL_PLAY_AREA.w)
		)
	get_parent().add_child.call_deferred(drop, true)
	
	# Only case needing special handling for now
	match chosen:
		DropTypes.POWERCELL:
			drop.SP_HEAL = SP_DROP_AMOUNT
		DropTypes.STIMPACK:
			drop.HP_HEAL = HP_DROP_AMOUNT
		DropTypes.WEAPON:
			drop.set_random()


# If overriden add super() at the end
func _update_params():
	hp = MAX_HP
	hp_bar.max_value = MAX_HP
	hp_bar.warning_above = MAX_HP + 1
	hp_bar.value = hp
	hp_bar.visible = HEALTHBAR_VISIBLE

func _set_type(t : Types):
	type = t

static func update_weights():
	drop_rng = WeightedRandom.new([
		GameConfig.Nothing_DROP_CHANCE,
		GameConfig.PowerCell_DROP_CHANCE,
		GameConfig.StimPack_DROP_CHANCE,
		GameConfig.Weapon_DROP_CHANCE,
	])
	type_rng = WeightedRandom.new([
		GameConfig.GrayVariant_SPAWN_CHANCE,
		GameConfig.RedVariant_SPAWN_CHANCE,
		GameConfig.GreenVariant_SPAWN_CHANCE,
		GameConfig.BlueVariant_SPAWN_CHANCE,
	])
