@tool
extends Marker2D

enum { EMPTY = -2, DEAD = -1 }

const portraits = {
	Character.Colors.RED : 0,
	Character.Colors.BLUE : 1,
	Character.Colors.PURPLE : 4,
	Character.Colors.YELLOW : 2,
	Character.Colors.GREEN : 5,
	Character.Colors.YELLOWRED : 3,
	DEAD: 6,
	EMPTY: 7,
}

const weapon_textures = {
	SkillDB.Effects.NORMAL: preload("res://assets/sfx/ui/UI_Weapons_Yellow.png"),
	SkillDB.Effects.BLAZING: preload("res://assets/sfx/ui/UI_Weapons_Orange.png"),
	SkillDB.Effects.CORROSION: preload("res://assets/sfx/ui/UI_Weapons_Green.png"),
	SkillDB.Effects.CRYO: preload("res://assets/sfx/ui/UI_Weapons_White.png"),
}

const weapon_frames = {
	SkillDB.Weapons.SWORD:0,
	SkillDB.Weapons.CHAIN:2,
	SkillDB.Weapons.BATON:1,
	SkillDB.Weapons.SHOTGUN:4,
	SkillDB.Weapons.RIFLE:3,
}


@export_range(-2,5) var portrait : int = EMPTY:
	set(v):
		portrait = v
		if !_is_ready:
			await ready
		portrait_sprite.frame = portraits[portrait]

@export var weapon_type : SkillDB.Weapons = SkillDB.Weapons.SWORD:
	set(v):
		weapon_type = v
		if !_is_ready:
			await ready
		weapon_sprite.frame = weapon_frames[weapon_type]

@export var weapon_effect : SkillDB.Effects = SkillDB.Effects.NORMAL:
	set(v):
		weapon_effect = v
		if !_is_ready:
			await ready
		weapon_sprite.texture = weapon_textures[weapon_effect]


@export_range(1.0, 10.99) var weapon_lvl : float = 1.0:
	set(v):
		weapon_lvl = v
		if !_is_ready:
			await ready
		var as_string = "%2d" % [(v-1) * 100 ]
		if v < 2.0:
			as_string = as_string.substr(0)
		weapon_lvl_text.text = as_string

@export var player_name : String = "Player":
	set(v):
		player_name = v
		if !_is_ready:
			await ready
		player_name_text.text = v if v.length() <= 10 else (v.substr(0,8) + "..")

@export var player_path : String:
	set(v):
		player_path = v
		player_ref = get_node_or_null(player_path)

var player_ref : Character = null

@onready var weapon_sprite : Sprite2D = $weapon
@onready var weapon_lvl_text : RetroText = $weapon_lvl
@onready var portrait_sprite : Sprite2D = $portrait
@onready var player_name_text : RetroText = $player_name

var _is_ready = false

func _ready() -> void:
	_is_ready = true
	

# TODO: use signals
func _process(delta: float) -> void:
	if !player_ref:
		return
	if player_ref.dead:
		portrait = DEAD
		player_name = "#OFFLINE#"
		return
	if player_name != player_ref.player_name:
		player_name = player_ref.player_name
	if portrait != player_ref.color:
		portrait = player_ref.color
	if weapon_effect != player_ref.equipped_effect:
		weapon_effect = player_ref.equipped_effect
	if weapon_type != player_ref.equipped_attack:
		weapon_type = player_ref.equipped_attack
	if weapon_lvl != player_ref.attack.get("dmg_multiplier"):
		weapon_lvl = player_ref.attack.get("dmg_multiplier")
