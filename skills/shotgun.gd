extends Skill

@export var DAMAGE = 5
var dmg_multiplier = 1.0
var effective_damage = DAMAGE

@onready var offset : Marker2D = $offset
@onready var sprite : AnimatedSprite2D = $offset/sprite
@onready var hitbox : HitboxArc = $offset/hitbox


func _ready() -> void:
	sprite.hide()
	$dmg_up.timeout.connect(func():
		dmg_multiplier += GameConfig.WeaponDMG_MULT_INCREASE_PER_SECOND
		effective_damage = DAMAGE * dmg_multiplier
		)
	super()


func _launch():
	if character.aim.x != 0:
		offset.position.x = sign(character.aim.x) * abs(offset.position.x)
	
	sprite.show()
	sprite.play()
	offset.rotation = character.aim.angle()
	hitbox.deal_damage(effective_damage, { "type" : "electric", "effect" : effect })
	await get_tree().create_timer(COOLDOWN).timeout
	sprite.hide()


func _update_params():
	DAMAGE = GameConfig.Shotgun_DAMAGE
	COOLDOWN = GameConfig.Shotgun_COOLDOWN
	var max_range_in_pixels : int = round(GameConfig.Shotgun_MAX_RANGE * Globals.PIXELS_PER_UNIT)
	hitbox.angle = GameConfig.Shotgun_SPREAD
	hitbox.range = max_range_in_pixels
	# This is based on current asset resolution. if the spread angle changes the asset will have to be redesigned
	sprite.scale = Vector2.ONE * (max_range_in_pixels / 77.0)
	pass


func _update_effect():
	sprite.animation = effect_name
