extends Button

func _ready():
	mouse_entered.connect(_mouse_entered)
	focus_entered.connect(_focus_entered)
	button_down.connect(_button_down)
	button_up.connect(_button_up)

func _mouse_entered():
	if not disabled and is_visible_in_tree():
		grab_focus()

func _focus_entered():
	if disabled or not is_visible_in_tree():
		release_focus()
		return
	$FocusEnterSFX.play()

func _button_down():
	$ButtonDownSFX.play()

func _button_up():
	# Only play up sound if mouse is outside button, so press will have no effect.
	if not Rect2(Vector2(), size).has_point(get_local_mouse_position()):
		$ButtonUpSFX.play()
