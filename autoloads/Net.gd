extends Node

var peer : ENetMultiplayerPeer
var force_local := false
var server_autoquit := true
var lobby

func disconnect_from_game():
	if not peer or peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		return
	peer.refuse_new_connections = true
	peer.close()
	peer = null
	multiplayer.multiplayer_peer = null
	if is_w4_cloud():
		W4GD.game_server.set_server_state(W4GD.game_server.ServerState.SHUTDOWN)

func host_game() -> Error:
	if Globals.is_headless() or force_local:
		return host_game_local()
	else:
		return await host_game_w4_cloud()

func host_game_w4_cloud() -> Error:
	var result = await W4GD.matchmaker.get_cluster_list().async()
	if result.is_error():
		return ERR_CANT_CONNECT
	var clusters : Array = result.as_array()
	if clusters.size() < 1:
		return ERR_UNAVAILABLE

	result = await W4GD.matchmaker.create_lobby(
		W4GD.matchmaker.LobbyType.DEDICATED_SERVER,
		{
			cluster = clusters.front(),
			max_players = PlayerConfig.net_max_players,
			props = {
				"max_players" = PlayerConfig.net_max_players,
				"version" = Globals.get_version(),
				"creator_name" = PlayerConfig.get_user_name()
			}
		}
	).async()
	if result.is_error():
		return ERR_CANT_CREATE
	lobby = result.get_data()

	lobby.state = W4GD.matchmaker.LobbyState.IN_PROGRESS
	lobby.received_server_ticket.connect(_on_received_server_ticket)
	result = await lobby.save().async()
	if result.is_error():
		return ERR_DATABASE_CANT_WRITE

	print("Created lobby ", lobby.id, ", waiting for server")
	W4GD.analytics.lobby_joined(lobby.id, { is_host = true })

	return OK

func set_lobby_done() -> Error:
	if not lobby:
		return ERR_DOES_NOT_EXIST
	lobby.state = W4GD.matchmaker.LobbyState.DONE
	var result = await lobby.save().async()
	if result.is_error():
		return ERR_DATABASE_CANT_WRITE
	return OK

func _on_received_server_ticket(ticket):
	print("Received server ticket for ", ticket.ip, ":", ticket.port)
	W4GD.game_server.start_client(W4GD.get_identity().get_uid(), ticket.secret, {})
	var e := _join_game(ticket.ip, ticket.port)
	if e != OK:
		Events.game_error.emit("Error joining game: " + error_string(e) + " (" + str(e) + ")")

func join_game(address_or_id) -> Error:
	address_or_id = address_or_id.strip_edges()
	if address_or_id != "localhost" and not "." in address_or_id:
		# address_or_id is game ID
		var result = await W4GD.matchmaker.join_lobby(address_or_id).async()
		if result.is_error():
			print("Error: ", result.as_error())
			return ERR_CANT_CONNECT
		lobby = result.get_data()
		print("Joined lobby ", address_or_id, ", waiting for server ticket")
		W4GD.analytics.lobby_joined(address_or_id, { is_host = false })
		lobby.received_server_ticket.connect(_on_received_server_ticket)
		if lobby.get_server_ticket():
			_on_received_server_ticket(lobby.get_server_ticket())
		return OK
	else:
		# address_or_id is IP address or hostname
		print("Joining local game")
		return _join_game(address_or_id)

func _join_game(address : String, port = W4Client.get_server_default_port()) -> Error:
	disconnect_from_game()
	peer = ENetMultiplayerPeer.new()
	var e = peer.create_client(address, port)
	if e != OK:
		print("Error connecting when joining game: ", str(e), " (" + error_string(e) + ")")
		return e
	multiplayer.multiplayer_peer = peer
	return OK

func host_game_local() -> Error:
	disconnect_from_game()
	var port = W4Client.get_server_default_port()
	print("Creating server on port ", port, ", max players: ", PlayerConfig.net_max_players)
	peer = ENetMultiplayerPeer.new()
	var e = peer.create_server(port, PlayerConfig.net_max_players)
	if e != OK:
		return e
	multiplayer.multiplayer_peer = peer
	if is_w4_cloud():
		W4GD.game_server.set_server_state(W4GD.game_server.ServerState.READY)
	return OK

func is_w4_cloud():
	return OS.has_environment('W4CLOUD') or '--w4cloud' in OS.get_cmdline_args()

func is_logged_in():
	return W4GD.get_identity().role != "anon"

func get_id():
	return W4GD.get_identity().get_uid()

# Returns true if currently in a online game in a lobby we are the creator of,
# or if this is a local or singleplayer game.
func is_lobby_creator(l=lobby):
	if not is_logged_in() or not l:
		return true
	return l.creator_id == get_id()

func should_autoquit():
	if not server_autoquit:
		return
	return is_w4_cloud() or Globals.is_headless()
