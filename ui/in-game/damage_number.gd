class_name DamageNumber
extends Node2D
## Renders rpg-style floating damage numbers
##
## This are used in a "fire and forget" manner
## Just do DamageNumber.new(...) and pass the parent, number, color and (optional) font size. No
## adding as a child and freeing are handled automatically

var _text : String = ""
var _font := preload("res://assets/fonts/pixellocale-v-1-4.ttf") #ThemeDB.fallback_font
var _font_size := 9
var _color := Color.WHITE
var _color_dark := _color
var _color_bright := _color
var _framecount : int = 0

func _init(parent: Node, p_position: Vector2, p_number : int, p_color : Color, font_size : int = 9):
	position = p_position.round()
	_text = str(p_number)
	_color = p_color
	_color_dark = p_color
	_color_bright = p_color.lightened(0.5)
	_font_size = font_size
	parent.add_child(self)

# handle blinking
func _process(_delta):
	if _framecount < 3:
		_framecount += 1
		return
	_framecount = 0
	if _color == _color_dark:
		_color = _color_bright
	else:
		_color = _color_dark
	queue_redraw()

# move up for 0.3 seconds then despawn
func _ready():
	z_index = Globals.IN_GAME_UI_Z_INDEX
	var tween = get_tree().create_tween()
	tween.set_ease(Tween.EASE_OUT)
	tween.tween_property(self, ^"position:y", position.y - 0.3 * Globals.PIXELS_PER_UNIT, 0.3)
	await tween.finished
	queue_free()

func _draw():
	draw_string_outline(_font, Vector2.ZERO, _text,HORIZONTAL_ALIGNMENT_CENTER, -1, _font_size, 2, Color.BLACK)
	draw_string(_font, Vector2.ZERO, _text,HORIZONTAL_ALIGNMENT_CENTER, -1, _font_size, _color)
