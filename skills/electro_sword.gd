extends Skill

@export var DAMAGE = 5
var dmg_multiplier = 1.0
var effective_damage = DAMAGE

@onready var bg : AnimatedSprite2D = $bg
@onready var fg : AnimatedSprite2D = $fg

var _anim_length := 1.0

func _ready():
	bg.hide()
	fg.hide()
	bg.animation_finished.connect(bg.hide)
	fg.animation_finished.connect(fg.hide)
	_anim_length = bg.sprite_frames.get_frame_count(effect_name) / float(bg.sprite_frames.get_animation_speed(effect_name))
	$dmg_up.timeout.connect(func():
		dmg_multiplier += GameConfig.WeaponDMG_MULT_INCREASE_PER_SECOND
		effective_damage = DAMAGE * dmg_multiplier
		)
	super()

func _launch():
	rotation = character.aim.angle()
	bg.show()
	fg.show()
	bg.play(effect_name)
	fg.play(effect_name)
	($hitbox as Hitbox).deal_damage( effective_damage, { "type" : "slash", "effect" : effect } )

func _process(_delta):
	bg.flip_v = character.aim.x < 0
	fg.flip_v = bg.flip_v

func _update_params():
	DAMAGE = GameConfig.ElectroSword_DAMAGE
	COOLDOWN = GameConfig.ElectroSword_COOLDOWN
	var speed_scale = _anim_length / COOLDOWN
	bg.speed_scale = speed_scale
	fg.speed_scale = speed_scale

func _on_bg_frame_changed():
	if bg.frame == 8:
		($hitbox as Hitbox).deal_damage( effective_damage )
