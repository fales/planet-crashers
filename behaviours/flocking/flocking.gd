extends Node

## Moves the parent node towards a goal and can be part of a flock.
##
## Flocking behaviour based on boids to move (somewhat) intelligently as a swarm
## towards a goal, based on boids and supporting avoidance.

## If set to false, this behaviour does nothing.
@export
var enabled := true

## Name of the Godot group to manage all members of this flock.
@export
var flock_group_name := "flock 1":
	set(v):
		if v != null and len(v) > 0 and v != flock_group_name:
			remove_from_group(flock_group_name)
			add_to_group(v)
			flock_group_name = v

## Will try to move to the nearest goal in this group.
@export
var goal_group_name := "players"

## Consider the goal reached if within this distance.
@export
var goal_range := 10

## Velocity, how fast this can move.
@export
var move_speed := 80

## Range to detect neighbours in flock, higher values mean more accurate flocks,
## but come at a large performance cost.
@export
var perception_radius := 48

## Whether to automatically update the parent Node's position. If disabled,
## the [code]position[/code]/[code]velocity[/code] values must be manually retrieved and handled.
@export
var update_parent_position := true

## Whether to update the parent Node's angle.
@export
var update_parent_angle := false

## Steering strength. Higher values result in faster adaptations of the heading,
## but also result in more overshooting.
@export
var steer_force := 50.0

## Flock/boid alignment force.
@export
var alignment_force := 1.2

## Flock/boid cohesion force.
@export
var cohesion_force := 0.5

## Flock/boid separation force.
@export
var separation_force := 1.0

## Flock/boid avoidance force.
@export
var avoidance_force := 30.0

## Flock/boid centralization force.
@export
var centralization_force := 0.5

@export
## Flock/boid centralization force radius.
## If the distance is greater, centralization will not apply.
var centralization_force_radius := 10

## Recalculate all flock vectors every X frames.
## Lower values make the swarm more responsive, but come at a high processing cost.
## Higher values make the swarm much less responsive. Test carefully!
@export
var recalc_flock_every := 5

## After the entity arrived at the goal, recalculate every X frames whether
## we are still near the goal or need to start moving again.
## This is useful to avoid processing when we reached the goal anyway.
@export
var recalc_after_arrival_every := 10

## Recalculate the current goal every X frames.
## This means every X frames, a new goal may be chosen if the old goal has
## become invalid or another goal is now nearer.
@export
var recalc_goal_every := 20

## Whether to switch to direct path seeking once we are arriving
## (i.e., once we are near to the goal).
@export
var arrival_direct_seek := true

## The distance from which to use direct path seeking if enabled.
@export
var arrival_distance = 100

## Random offset that is applied to the goal position when using direct path
## seeking. The offset is randomly chosen once inside this range.
## Useful to avoid many swarm members perfectly overlapping.
@export
var arrival_random_offset = 10

## Whether the goal is currently reached.
var goal_reached := false

var goal_distance := 0.0
var nearest_goal = null
var velocity := Vector2()
var position := Vector2()
var acceleration := Vector2()
var arrival_calc_offset := Vector2()

func _ready():
	add_to_group(flock_group_name)
	arrival_calc_offset = Vector2(
		randf_range(-arrival_random_offset, +arrival_random_offset),
		randf_range(-arrival_random_offset, +arrival_random_offset),
	)

func _process(delta):
	if not is_multiplayer_authority():
		return

	if not enabled:
		return

	if goal_reached and Engine.get_process_frames() % recalc_after_arrival_every != 0:
		return

	if "hp" in get_parent() and get_parent().hp <= 0:
		return

	position = get_parent().position

	if _is_valid_goal(nearest_goal):
		goal_distance = nearest_goal.position.distance_to(position)
		if goal_distance < goal_range:
			goal_reached = true
			return
		goal_reached = false
		if arrival_direct_seek and goal_distance < arrival_distance:
			get_parent().position = position.move_toward(
				nearest_goal.position+arrival_calc_offset, move_speed * delta)
			return
	goal_reached = false

	if update_parent_position:
		get_parent().translate(velocity * delta)
	if update_parent_angle:
		get_parent().rotation = velocity.angle()

	if Engine.get_process_frames() % recalc_flock_every != 0:
		return

	if nearest_goal == null or Engine.get_process_frames() % recalc_goal_every:
		nearest_goal = _get_nearest_in_group(goal_group_name)

	var neighbors = _get_group_in_radius(flock_group_name, perception_radius)

	acceleration += _process_alignments(neighbors) * alignment_force
	acceleration += _process_cohesion(neighbors) * cohesion_force
	acceleration += _process_separation(neighbors) * separation_force

	if nearest_goal != null:
		acceleration += _process_centralization(nearest_goal.position) * centralization_force

	velocity += acceleration * delta
	velocity = velocity.limit_length(move_speed)

func _is_valid_goal(goal) -> bool:
	if goal == null or not "position" in goal:
		return false
	if "hp" in goal and goal.hp <= 0:
		return false
	return true

func _process_alignments(neighbors) -> Vector2:
	var vector = Vector2()
	if neighbors.is_empty():
		return vector

	for n in neighbors:
		vector += n.velocity
	vector /= neighbors.size()

	return _steer(vector.normalized() * move_speed)

func _process_cohesion(neighbors) -> Vector2:
	var vector = Vector2()
	if neighbors.is_empty():
		return vector
	for n in neighbors:
		vector += n.position
	vector /= neighbors.size()

	return _steer((vector - position).normalized() * move_speed)

func _process_separation(neighbors) -> Vector2:
	var vector = Vector2()
	var close_neighbors = []

	for n in neighbors:
		if position.distance_to(n.position) < perception_radius / 2.0:
			close_neighbors.push_back(n)
	if close_neighbors.is_empty():
		return vector

	for n in close_neighbors:
		var difference = position - n.position
		vector += difference.normalized() / difference.length()

	vector /= close_neighbors.size()

	return _steer(vector.normalized() * move_speed)

func _process_centralization(center: Vector2) -> Vector2:
	if position.distance_to(center) < centralization_force_radius:
		return Vector2()
	return _steer((center - position).normalized() * move_speed)	

func _steer(target, force=steer_force) -> Vector2:
	var steer = target - velocity
	steer = steer.normalized() * force

	return steer

func _get_group_in_radius(group_name, view_radius) -> Array:
	var neighbors = []

	for neighbor in get_tree().get_nodes_in_group(group_name):
		if position.distance_to(neighbor.position) <= view_radius and not neighbor == self:
			if "goal_reached" in neighbor and neighbor.goal_reached:
				continue
			neighbors.push_back(neighbor)

	return neighbors

func _get_nearest_in_group(group_name):
	var nearest = null
	for n in get_tree().get_nodes_in_group(group_name):
		if not _is_valid_goal(n):
			continue
		if nearest == null or n.position.distance_to(position) < nearest.position.distance_to(position):
			nearest = n
	return nearest
