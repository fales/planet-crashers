extends Item

@export var SP_HEAL : int = 10

func _apply_effect(target : Area2D) -> void:
	target.owner.sp_damage(-SP_HEAL)
