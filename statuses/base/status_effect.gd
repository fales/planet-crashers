extends Node2D

signal finished

var parent : Enemy

var duration_sec : float = 5.0
var proc_interval_sec : float = 1.0

func start():
	$duration_timer.start(duration_sec)
	$proc_timer.start(proc_interval_sec)


func _on_proc_timer_timeout() -> void:
	proc()


func _on_duration_timer_timeout() -> void:
	end()

# Override
func proc():
	pass


# Override
func process_damage(amount : int, custom_data := {}) -> int:
	return amount

func end():
	finished.emit()
	queue_free()
