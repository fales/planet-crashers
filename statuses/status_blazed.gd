extends "res://statuses/base/status_effect.gd"

func start():
	duration_sec = GameConfig.StatusBlazed_DURATION
	proc_interval_sec = GameConfig.StatusBlazed_PROC_INTERVAL
	super()


func proc():
	($Hitbox as Hitbox).deal_damage(GameConfig.StatusBlazed_EXPLOTION_DMG)
	var boom_fx = preload("res://fx/explosion.tscn").instantiate()
	parent.get_parent().add_child(boom_fx)
	boom_fx.scale = Vector2.ONE * randf_range(0.5, 1.0)
	boom_fx.global_position = global_position + Vector2(randf_range(-16,16),randf_range(-8,8))
	pass
