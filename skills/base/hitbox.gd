@tool
class_name Hitbox
extends ShapeCast2D
## A ShapeCast2D that can deal damage to to the objects it hits
##
## Can be set to target enemies or players by setting [member is_enemy]
##
## Meant for melee swings or anything that needs to be reoriented/repositioned and
## check for collisions in the same frame. Otherwise you can use [HitboxArea]


func _init():
	enabled = false
	if target_position != Vector2.ZERO:
		target_position = Vector2.ZERO
		push_warning("Ivalid target_position. It must be (0,0) for force_shapecast_update() to work. Reset to correct value")
	collide_with_areas = true
	collide_with_bodies = false
	is_enemy = is_enemy # Setter doesn't seem to trigger automatically as in Godot 3

## Sets the proper collision mask depending on what objects it should target
@export var is_enemy = true :
	set(value):
		is_enemy = value
		# If enemy target players else target enemies
		collision_mask = Globals.PLAYER_COLLISION_LAYER if is_enemy else Globals.ENEMY_COLLISION_LAYER

## Forces a shapecast update and calls the damage method on the owner of any
## [Hurtbox] instances it hits
## [param amount]: amount of damage to cause
## [param custom_data]: A dictionary with any extra info about the damage. Currently used for 
## status effects and damage type
func deal_damage( amount : int, custom_data:= {} ) -> int:
	force_shapecast_update()
	var target_count = get_collision_count()
	for i in target_count:
		get_collider(i).owner.damage(amount, custom_data)
	return target_count
