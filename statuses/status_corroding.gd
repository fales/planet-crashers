extends "res://statuses/base/status_effect.gd"

# weapon damage
var base_damage = 2

func start():
	duration_sec = GameConfig.StatusCorroding_DURATION
	proc_interval_sec = GameConfig.StatusCorroding_PROC_INTERVAL
	super()

func proc():
	parent.damage(ceil(base_damage * GameConfig.StatusCorroding_DMG_MULTIPLIER))
