extends "res://statuses/base/status_effect.gd"

var old_speed

func start():
	old_speed = parent.SPEED
	parent.SPEED *= GameConfig.StatusCryo_SPEED_MULTIPLIER
	duration_sec = GameConfig.StatusCryo_DURATION
	super()

func end():
	parent.SPEED = old_speed
	super()

func process_damage(amount : int, _custom_data := {}):
	if randf() < GameConfig.StatusCryo_SHATTER_CHANCE:
		@warning_ignore("narrowing_conversion")
		amount *= GameConfig.StatusCryo_SHATTER_DMG_MULTIPLIER
	return amount
