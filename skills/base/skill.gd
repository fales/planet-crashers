class_name Skill
extends Node2D
## Skill class representing a player action/attack/move
##
## Any player action other than regular movement can be a skill.
##
## All skills have a cost in special points: [member SP_COST],
## a cooldown time: [member COOLDOWN] and an overridable method that
## gets called whenever they are activated: [method _launch]
## 
## Everything else is considered skill-specific and defined in subclasses

## Name of the skill. Can be used for debugging or to display in the game UI
@export var DISPLAY_NAME : String = "Unnamed Skill"

## Icon representing the skill. Meant for use in the game UI
@export var ICON : Texture

## Cost of use in special points
@export var SP_COST : int = 10

## Cooldown in seconds
@export var COOLDOWN : float = 1.0:
	set(val):
		COOLDOWN = max(val, 0.017)
		if !_is_ready:
			await ready
		timer.wait_time = COOLDOWN
		# Always stop the timer when the cooldown amount changes
		timer.stop()

## Cooldown timer
@onready var timer : Timer = $Timer

## Holds a reference to character having this skill instance equipped.
##
## Can be used for self heal or anything that affects or needs extra info
## from the caster
var character : Character

## Used for skills that should look different depending on the color palette of
## the player. Override [method _update_color] to implement custom behavior
var color : Character.Colors = Character.Colors.RED:
	set(val):
		color = val
		color_suffix = COLOR_SUFFIXES[val]
		_update_color() #skill specific behavior here


## Used for animation per-color-palette animation sets
var color_suffix = "_green"

## Mapping of color enum to string suffixes to use in animation sets
const COLOR_SUFFIXES := {
	Character.Colors.RED: "_green",
	Character.Colors.BLUE: "_yellow",
	Character.Colors.PURPLE: "_yellow",
	Character.Colors.YELLOW: "_green",
	Character.Colors.GREEN: "_yellow",
	Character.Colors.YELLOWRED: "_green",
}

## Status effect that the skill applies to the target
var effect : SkillDB.Effects = SkillDB.Effects.NORMAL:
	set(val):
		effect = val
		effect_name = EFFECT_NAMES[val]
		if !_is_ready:
			await ready
		_update_effect() #skill specific behavior here

## Used mostly for effect specific animations
var effect_name = "normal"

## Mapping of effect enum to strings to use in animation
const EFFECT_NAMES := {
	SkillDB.Effects.BLAZING: "blazing",
	SkillDB.Effects.CRYO: "cryo",
	SkillDB.Effects.CORROSION: "corrosion",
	SkillDB.Effects.NORMAL: "normal",
}

## Remaining cooldown time
var cooldown_remaining:
	get:
		if !_is_ready:
			return INF
		return timer.time_left

## True if [member cooldown_remaining] is zero
var is_available:
	get:
		return _is_ready and timer.time_left == 0

## Emited whenever the cooldown timer reaches zero
signal available()

## Set to true when the node calls _ready. Used by setters that
## need to access child nodes
var _is_ready := false

func _init() -> void:
	# force trigger setter
	effect = effect

func _ready():
	_is_ready = true
	timer.timeout.connect(func(): self.available.emit() )
	assert(character != null)
	_update_params()
	GameConfig.updated.connect(_update_params)

## Activates the attack and starts the cooldown timer.
##
## Notice that it will always launch the attack.
## Honoring the cooldown is up to the user.  You can use [member is_available],
## [member cooldown_remaining] and [signal available]
func launch():
	timer.start()
	_launch()


## Called by [method launch]. Override with skill specific code 
func _launch():
	printerr("Skill '%s': _launch not implemented" % DISPLAY_NAME)

## Called whenever the game config changes. Updates all relevant properties to the
## new values defined by the game designer. Override with skill specific code 
func _update_params():
	printerr("Skill '%s': _udpate_params not implemented" % DISPLAY_NAME)

## Called when setting the skill color palette (see [member color]). Override with skill specific logic
func _update_color():
	pass

## Called when updating the skill effect (see [member effect]). Override with skill specific logic
func _update_effect():
	pass
