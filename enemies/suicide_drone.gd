extends Enemy

# Currently both combat_operative.tscn and suicide_drone.tscn use this script since they seem to have
# the same behavior

@export var DAMAGE : int = 10

var skip_spawn_animation := true

var _last_pos : Vector2 = position

var _destination : Vector2 = Vector2.ZERO
var _velocity : Vector2 = Vector2.ZERO

enum States { WAITING, MOVING, DYING }
var _current_state = States.WAITING

var _wait_timer : float = 0.0

@onready var hurtbox : Hurtbox = $visuals/hurtbox
@onready var hitbox : HitboxArea = $visuals/hitbox


func _ready():
	super()
	hide() # SPAWN HACK. REMOVE PLZ
	_update_destination()


# @Max: Added this to have something somewhat similar to what Fer requested. Feel free to remove
# this code if you implement it as a behavior node
func _physics_process(delta):
	if not is_multiplayer_authority():
		return
	match _current_state:
		States.MOVING:
			# BEGIN SPAWN HACK. REMOVE PLZ
			if visible == false:
				position = _destination
				_current_state = States.WAITING
				show()
			# END SPAWN HACK
			if _destination.distance_to(position) < 0.1 * Globals.PIXELS_PER_UNIT:
				_wait_timer = randf_range(0.0,5.0)
				_current_state = States.WAITING
			else:
				_velocity = _velocity.lerp(position.direction_to(_destination) * SPEED * delta, 0.1)
				translate(_velocity)
				_update_facing()
		States.WAITING:
			if _wait_timer <= 0.0:
				_update_destination()
				_current_state = States.MOVING
			else:
				_wait_timer -= delta
		States.DYING:
			_velocity = _velocity.lerp(Vector2.ZERO, 0.05)
			translate(_velocity)


func _update_destination():
	if not is_multiplayer_authority():
		return
	if position.x <= 0:
		_destination.x = Globals.LEVEL_SIZE.x + 5 * Globals.PIXELS_PER_UNIT
	elif position.x >= Globals.LEVEL_SIZE.x:
		_destination.x = -5 * Globals.PIXELS_PER_UNIT
	else: # No longer necessary if they don't spawn in the middle of the level
		var go_left = [false,true].pick_random()
		printt(go_left, float(go_left), float(!go_left))
		_destination.x = float(go_left) * (-5 * Globals.PIXELS_PER_UNIT) + float(!go_left) * (Globals.LEVEL_SIZE.x + 5 * Globals.PIXELS_PER_UNIT)
	_destination.y = randf_range(Globals.LEVEL_PLAY_AREA.y, Globals.LEVEL_PLAY_AREA.w )
	position.y = _destination.y


func _update_facing():
	var facing = sign(position.x - _last_pos.x)
	_last_pos = position
	if facing != 0:
		$visuals.scale.x = sign(facing)


func die():
#	if not is_multiplayer_authority():
#		return
	_current_state = States.DYING
	hitbox.set_deferred("monitoring", false)
	await get_tree().create_timer(0.3).timeout
	$visuals.hide()
	if has_node("shadow"):
		$shadow.hide()
	var boom_fx = preload("res://fx/explosion.tscn").instantiate()
	add_child(boom_fx)
	boom_fx.global_position = $visuals.global_position
	hurtbox.set_deferred("monitorable", false)
	hitbox.set_deferred("monitoring", false)
	await boom_fx.animation_finished
	super()

func _on_hitbox_target_acquired(_target):
	if not is_multiplayer_authority():
		return
	hitbox.deal_damage(DAMAGE, {"direction" : global_position.direction_to(_target.global_position)})
	die()

func _set_type(t : Types):
	super(t)
	if !_is_ready:
		await ready
	var mat : Material
	match t:
		Types.RED:
			mat = preload("res://enemies/recolors/red.material")
		Types.BLUE:
			mat = preload("res://enemies/recolors/blue.material")
		Types.GRAY:
			mat = preload("res://enemies/recolors/gray.material")
		Types.GREEN:
			mat = null
	$visuals/sprite.material = mat

func _update_params():
	MAX_HP = GameConfig.SuicideDrone_MAX_HP
	SPEED = GameConfig.SuicideDrone_SPEED * Globals.PIXELS_PER_UNIT
	SP_DROP_AMOUNT = GameConfig.SuicideDrone_SP_DROP_AMOUNT
	HP_DROP_AMOUNT = GameConfig.SuicideDrone_HP_DROP_AMOUNT
	DAMAGE = GameConfig.SuicideDrone_DAMAGE
	super()
