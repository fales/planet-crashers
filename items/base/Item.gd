extends HitboxArea
class_name Item

signal picked_up

const FALL_AMOUNT : int = 20

var consumed := false

## Time after which the item starts blinking before despawn
var despawn_time = GameConfig.Item_DESPAWN_TIME
## Time for which the item blinks before it then despawns
var blink_time = 1 if despawn_time > 2 else despawn_time / 3.0

var _framecount = 4

func _ready():
	super()
	picked_up.connect(queue_free)
	set_process(false)
	var tween := get_tree().create_tween()
	position.y -= FALL_AMOUNT
	target_acquired.connect(_on_target_acquired)
	tween.tween_property(self, ^"position:y", position.y+FALL_AMOUNT, 0.15)
	tween.set_ease(Tween.EASE_IN)
	await tween.finished
	set_deferred("monitoring", true)

	if despawn_time > 0:
		await get_tree().create_timer(despawn_time-blink_time).timeout
		set_process(true)
		await get_tree().create_timer(blink_time).timeout
		queue_free()

# Frame-based blinking to avoid aliasing issues on lower framerates
func _process(_delta: float) -> void:
	if _framecount:
		_framecount -= 1
	else:
		_framecount = 4
		visible = !visible

func _on_target_acquired(target : Area2D) -> void:
	if consumed or not target.is_multiplayer_authority():
		return
	consumed = true
	_apply_effect(target)
	
	_picked_up.rpc()
	Audio.sfx_play.rpc("res://assets/sfx/item/pickup.wav")
	queue_free()


@rpc("any_peer", "call_local", "reliable")
func _picked_up():
	picked_up.emit()


@warning_ignore("unused_parameter")
func _apply_effect(target : Area2D) -> void:
	printerr("Unimplemented")
