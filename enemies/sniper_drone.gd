extends Enemy

@export var DAMAGE : int = 30
@export var FLEE_RANGE : float = 160

@onready var attack_area : HitboxArea = $attack_area
@onready var hurtbox : Hurtbox = $visuals/hurtbox

var _destination : Vector2
var _velocity : Vector2 = Vector2.ZERO

enum States { PATROLLING, ENGAGING, FLEEING, DYING }
var _current_state = States.PATROLLING

var _current_target : Hurtbox:
	set(v):
		if _current_target == v:
			return
		_current_target = v
		if !_is_ready:
			await ready
		$visuals/targeting_ray.target = _current_target
		$visuals/targeting_ray.queue_redraw()
		$attack_timer.start()

func _ready() -> void:
	super()
	_update_destination()

func _physics_process(delta: float) -> void:
	if _current_target:
		$visuals/targeting_ray.radius = remap($attack_timer.time_left, $attack_timer.wait_time, 0, 32, 1)
		$visuals/targeting_ray.queue_redraw()
		var t_aim = target_aim * -$visuals.scale.x
		# Rotate the head a bit towards the player. Use 0.09 intervals so it looks better with pixelart style
		$visuals.rotation = round(clamp(t_aim.angle(), -0.07 * TAU, 0.04 * TAU) / 0.09) * 0.09
		$visuals/tentacles.global_rotation = 0.5 * TAU * float($visuals.scale.x < 0)
		$visuals/sprite/shot_fx.global_rotation = t_aim.angle() + 0.5 * TAU * float($visuals.scale.x < 0)
	else:
		$visuals.rotation = 0
		$visuals/tentacles.rotation = 0
	match _current_state:
		States.PATROLLING:	
			if position.distance_to(_destination) < 0.1 * Globals.PIXELS_PER_UNIT:
				_update_destination()
			target_aim = position.direction_to(_destination)
			_velocity = _velocity.slerp(target_aim * 0.5 * SPEED * delta, 0.03)
			_bounce_on_limits()
			translate(_velocity)
			_update_facing()
		States.ENGAGING:
			_current_target = _pick_weakest_target()
			if not _current_target:
				_current_state = States.PATROLLING
				return
			target_aim = global_position.direction_to(_current_target.global_position)
			_velocity = _velocity.slerp(target_aim.rotated(0.15 * TAU) * SPEED * delta, 0.03)
			_bounce_on_limits()
			translate(_velocity)
			_update_facing()
			for t in attack_area.targets:
				if global_position.distance_to(t.global_position) < FLEE_RANGE:
					_current_state = States.FLEEING
					break
		States.FLEEING:
			var closest : Hurtbox = attack_area.targets.keys()[0]
			var min_distance : float = INF
			for t in attack_area.targets:
				var distance = global_position.distance_to(t.global_position)
				if distance < min_distance:
					min_distance = distance
					closest = t
			target_aim = global_position.direction_to(closest.global_position)
			_velocity = _velocity.slerp(-target_aim.rotated(0.15 * TAU) * SPEED * delta, 0.03)
			_bounce_on_limits()
			translate(_velocity)
			_update_facing()
		States.DYING:
			_current_target = null
			_velocity = _velocity.lerp(Vector2.ZERO, 0.1)
			translate(_velocity)

func _bounce_on_limits():
	if position.x < Globals.LEVEL_PLAY_AREA.x:
		_velocity.x = abs(_velocity.x)
	elif position.x > Globals.LEVEL_PLAY_AREA.z: 
		_velocity.x = -abs(_velocity.x)
	if position.y < Globals.LEVEL_PLAY_AREA.y:
		_velocity.y = abs(_velocity.y)
	elif position.y > Globals.LEVEL_PLAY_AREA.w:
		_velocity.y = -abs(_velocity.y)

func _update_facing():
	var facing = sign(target_aim.x)
	if facing != 0:
		$visuals.scale.x = -sign(facing)

func _set_type(t : Types):
	super(t)
	if !_is_ready:
		await ready
	var mat : Material
	match t:
		Types.RED:
			mat = preload("res://enemies/recolors/red.material")
		Types.BLUE:
			mat = preload("res://enemies/recolors/blue.material")
		Types.GRAY:
			mat = preload("res://enemies/recolors/gray.material")
		Types.GREEN:
			mat = null
	$visuals/sprite.material = mat

func _update_destination():
	_destination = Vector2(
		randf_range(Globals.LEVEL_PLAY_AREA.x, Globals.LEVEL_PLAY_AREA.z),
		randf_range(Globals.LEVEL_PLAY_AREA.y, Globals.LEVEL_PLAY_AREA.w)
		)
	print(_destination)


func die():
	_current_state = States.DYING
	$visuals.hide()
	if has_node("shadow"):
		$shadow.hide()
	var boom_fx = preload("res://fx/explosion.tscn").instantiate()
	add_child(boom_fx)
	boom_fx.global_position = $visuals.global_position
	hurtbox.set_deferred("monitorable", false)
	attack_area.set_deferred("monitoring", false)
	await boom_fx.animation_finished
	super()

func _on_attack_area_target_acquired(_target) -> void:
	if _current_state == States.DYING:
		return
	if _current_state == States.PATROLLING:
		_current_state = States.ENGAGING


func _on_attack_area_target_lost(_target) -> void:
	if _current_state == States.DYING:
		return
	if attack_area.targets.is_empty():
		_destination = _current_target.global_position if _current_target else global_position
		_current_target = null
		_current_state = States.PATROLLING
	pass # Replace with function body.

func _has_targets() -> bool:
	return !attack_area.targets.is_empty()

func _pick_weakest_target() -> Hurtbox:
	if attack_area.targets.is_empty():
		return null
	var weakest = attack_area.targets.keys()[0]
	for target in attack_area.targets:
		if target.owner.hp > 0 and target.owner.hp < weakest.owner.hp:
			weakest = target
	return weakest


func _on_attack_timer_timeout() -> void:
	if _current_target:
		_velocity = -target_aim * 0.08 * Globals.PIXELS_PER_UNIT
		$visuals/sprite/shot_fx.play("default")
		_current_target.owner.damage(DAMAGE, {"direction" : global_position.direction_to(_current_target.global_position)})
		var boom_fx = preload("res://fx/explosion.tscn").instantiate()
		add_child(boom_fx)
		boom_fx.global_position = _current_target.global_position
	pass # Replace with function body.


func _update_params():
	MAX_HP = GameConfig.SniperDrone_MAX_HP
	SPEED = GameConfig.SniperDrone_SPEED * Globals.PIXELS_PER_UNIT
	SP_DROP_AMOUNT = GameConfig.SniperDrone_SP_DROP_AMOUNT
	HP_DROP_AMOUNT = GameConfig.SniperDrone_HP_DROP_AMOUNT
	DAMAGE = GameConfig.SniperDrone_DAMAGE
	ATTACK_RANGE = GameConfig.SniperDrone_ATTACK_RADIUS * Globals.PIXELS_PER_UNIT
	($attack_area/collision.shape as CircleShape2D).radius = ATTACK_RANGE
	FLEE_RANGE = GameConfig.SniperDrone_FLEE_RADIUS * Globals.PIXELS_PER_UNIT
	super()
