extends Node2D

var player_type = preload("res://characters/character.tscn")
var player_info_scene = preload("res://ui/in-game/player_info.tscn")

@export var ticks_per_second := 1
@export var players : Node
@export var enemies : Node
@export var spawner : Node
@export var ui_player_info : Node
@export var control_scheme : Node

var _game_over := false
var _players = {}
var _player_colors = {}
const _player_colors_num = 6

var grid_enabled = false:
	set(v):
		grid_enabled = v
		queue_redraw()

func _ready():
	# Level logic should only run on server
	if not multiplayer.is_server() or multiplayer.multiplayer_peer is OfflineMultiplayerPeer:
		return

	for color in range (0, _player_colors_num):
		_player_colors[color] = 0

	Events.game_restart.connect(_reset)

	for id in multiplayer.get_peers():
		_add_player(id)

	multiplayer.peer_connected.connect(_add_player)
	multiplayer.peer_disconnected.connect(_remove_player)
	Events.round_begin.connect(_round_begin)

	# Add local host player if not running as a dedicated server
	if not Globals.is_headless():
		_add_player(1)

	_spawn_start_game_pickup()
	
	spawner.all_spawns_finished.connect(_game_won)

	await get_tree().create_timer(4).timeout
	get_tree().create_timer(1.0/float(ticks_per_second)).timeout.connect(_game_tick)


func _spawn_start_game_pickup():
	var start_game_pickup = load("res://items/power_cell.tscn").instantiate()
	start_game_pickup.position = Globals.LEVEL_SIZE / 2
	start_game_pickup.picked_up.connect(_rpc_start_game)
	start_game_pickup.despawn_time = 0
	$entities/enemies.add_child.call_deferred(start_game_pickup, true)

func _reset():
	for enemy in get_node("entities/enemies").get_children():
		enemy.queue_free()

	if not is_multiplayer_authority():
		return

	for player in get_tree().get_nodes_in_group("players"):
		player.request_despawn()
		player.queue_free()
		for p in get_node("entities/players").get_children():
			p.queue_free()
	
	for player in multiplayer.get_peers():
		print("Requesting respawn for ID: ", player)
		get_tree().create_timer(2).timeout.connect(_spawn_player.bind(player))

	await get_tree().create_timer(4).timeout
	_game_over = false
	get_tree().create_timer(1.0/float(ticks_per_second)).timeout.connect(_game_tick)
	_spawn_start_game_pickup()

func _round_begin(round : int, duration_seconds : int):
	if _game_over or not is_multiplayer_authority() or round < 1 or not Globals.resurrect_dead_on_new_round:
		return
	for player in get_tree().get_nodes_in_group("players"):
		if "hp" in player and player.hp < 1:
			var id := player.get_multiplayer_authority()
			player.request_despawn()
			get_tree().create_timer(1.5).timeout.connect(_spawn_player.bind(id))


func _rpc_start_game():
	_start_game.rpc()


@rpc("any_peer", "call_local", "reliable")
func _start_game():
	Events.game_begin.emit()


func _add_player(id : int):
	if _players.has(id):
		return
	print("Player connected: ", id, ", players: ", players.get_children())
	_players[id] = true
	_spawn_player(id)

func _get_next_color():
	var least_used_color = 0
	var least_used_count = _player_colors[0]
	if least_used_count > 0:
		for color in range (1, _player_colors_num):
			if _player_colors[color] < least_used_count:
				least_used_count = _player_colors[color]
				least_used_color = color
	print("Assigning player color ", least_used_color)
	_player_colors[least_used_color] = _player_colors[least_used_color] + 1
	return least_used_color

func _spawn_player(id : int):
	print("Spawning player for ID ", id)
	var color = _get_next_color()
	var char : Character = player_type.instantiate()
	char.name = str(id)
	char.color = color
	char.visible = false
	players.add_child(char)
	var player_info = player_info_scene.instantiate()
	player_info.name = char.name
	ui_player_info.add_child(player_info)
	player_info.player_path = char.get_path()
	_update_player_info_position()
	_update_control_scheme()

func _remove_player(id : int):
	_players.erase(id)
	print("Removing player: ", id, ", players: ", players.get_children(), " (" + str(_players.size()) + " remaining)")
	var p = players.find_child(str(id), false, false)
	if p:
		if "color" in p:
			_player_colors[p.color] = _player_colors[p.color] - 1
		p.queue_free()
		print("Removed ", id)
	else:
		print("No character found to remove for player ", id)
	var p_info = ui_player_info.find_child(str(id), false, false)
	if p_info:
		ui_player_info.remove_child(p_info)
		p_info.queue_free()
		_update_player_info_position()
	if _players.is_empty() and Net.should_autoquit():
		print("Last player quit, server exiting...")
		W4GD.analytics.stop_session()
		await W4GD.analytics.flush()
		if Net.is_w4_cloud():
			var err = await Net.set_lobby_done()
			if err != OK:
				print("Error setting lobby to done: ", error_string(err))
			W4GD.game_server.set_server_state(W4GD.game_server.ServerState.SHUTDOWN)
		Events.game_over.emit(0, 0, 0)
		await get_tree().create_timer(2).timeout
		get_tree().quit()

func _update_player_info_position():
	for i in ui_player_info.get_child_count():
		ui_player_info.get_child(i).position = Vector2(64 + 140 * i,4)

func _update_control_scheme():
	for player in players.get_children():
		if (player as Character).is_controllable:
			control_scheme.player_ref = player

func _game_tick():
	if _game_over:
		return

	_game_over = true
	var player_num = 0
	var players_alive = 0
	for player in get_tree().get_nodes_in_group("players"):
		player_num += 1
		if not "hp" in player or player.hp > 0:
			if "hp" in player and player.hp > 0:
				players_alive += 1
			_game_over = false
			break
	if player_num < 1:
		_game_over = false

	if _game_over:
		_emit_game_over.rpc(player_num, players_alive, spawner.get_round())
		return

	spawner.tick()
	get_tree().create_timer(1.0/float(ticks_per_second)).timeout.connect(_game_tick)


func _game_won():
	var player_num = 0
	var players_alive = 0
	for player in get_tree().get_nodes_in_group("players"):
		player_num += 1
		if not "hp" in player or player.hp > 0:
			if "hp" in player and player.hp > 0:
				players_alive += 1
	_emit_game_over.rpc(player_num, players_alive, spawner.get_round())


@rpc("reliable", "call_local")
func _emit_game_over(players, players_alive, round):
	Events.game_over.emit(players, players_alive, round)


func _draw() -> void:
	if !grid_enabled:
		return
	for i in 12:
		draw_line(Vector2(0, i * 32), Vector2(640, i * 32), Color(1, 1, 1, 0.1), 0.4, true)
	for i in 20:
		draw_line(Vector2(i * 32,0), Vector2(i * 32,360), Color(1, 1, 1, 0.1), 0.4, true)

	
