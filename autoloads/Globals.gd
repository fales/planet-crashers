## A place for any constants, variables or function that need to be available
## from anywhere and don't have a better place yet
extends Node

const APP_VERSION = "1.0"
const APP_VERSION_COMMIT = "{%CI_COMMIT_SHORT_SHA}"
const APP_VERSION_FORMAT = "v" + APP_VERSION + "{suffix}"

## All motion can be described in this unit in case we change
## resolution. Should be equivalent to the average height of a player chacter
const PIXELS_PER_UNIT := 32

## Health bars, damage numbers, etc. shuold be above the level shading
const IN_GAME_UI_Z_INDEX := 2000

const LEVEL_SIZE := Vector2(640,360)
const LEVEL_PLAY_AREA := Vector4(20, 28, 620, 350)

const ENEMY_COLLISION_LAYER := 4
const PLAYER_COLLISION_LAYER := 2

const round_timer_enabled := false
const resurrect_dead_on_new_round := true

func has_commit():
	return not APP_VERSION_COMMIT.contains("_SHA")

func get_commit():
	return APP_VERSION_COMMIT.substr(0, min(8, len(APP_VERSION_COMMIT)-1))
	
func popup(msg="Error", title="Error", ok="OK", cancel=""):
	print(title + ": " + msg + " (" + ok + "/" + cancel + ")")
	if is_headless():
		return

	var a
	if len(cancel) > 0:
		a = ConfirmationDialog.new()
	else:
		a = AcceptDialog.new()
	a.title = title
	a.dialog_text = msg
	a.exclusive = true
	a.ok_button_text = ok
	if len(cancel) > 0:
		a.cancel_button_text = cancel
	a.canceled.connect(a.queue_free)
	a.confirmed.connect(a.queue_free)
	add_child(a)
	a.popup_centered()
	a.get_ok_button().grab_focus()
	return a

func get_version() -> String:
	var suffix := "-"
	if Globals.has_commit():
		suffix += Globals.get_commit()
	else:
		suffix = "-dev"

	if OS.has_feature("editor"):
		suffix += " (Editor)"

	return Globals.APP_VERSION_FORMAT.format({"suffix": suffix})

func is_headless():
	return DisplayServer.get_name() == "headless"


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("debug_play"):
		get_tree().change_scene_to_file("res://level/level.tscn")


func get_cli_arg(key : String) -> String:
	for argument in OS.get_cmdline_args():
		argument = argument.strip_edges().lstrip("--").lstrip("-")
		var value = ""
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			argument = key_value[0]
			value = key_value[1]
		if argument != key:
			continue
		return value
	return ""
