extends HBoxContainer

signal back_to_main_menu()
signal host_game()
signal join_game()

var changelog_focus_style
var last_focused_button
var changelog_was_focused = false

func _ready():
	$vbox_left/button_play.grab_focus()
	last_focused_button = $vbox_left/button_play

	$vbox_left/button_play.pressed.connect(_button_play_pressed)
	$vbox_left/button_join.pressed.connect(_button_join_pressed)
	$vbox_left/button_quit.pressed.connect(get_tree().quit)
	$vbox_left/button_credits.pressed.connect(_button_credits_pressed)

	$changelog_panel/margin/changelog.visibility_changed.connect(_handle_changelog_focus)
	$changelog_panel/margin/changelog.focus_entered.connect(_handle_changelog_focus)
	$changelog_panel/margin/changelog.focus_exited.connect(_handle_changelog_focus)

	$vbox_left/button_logout.pressed.connect(_button_logout_pressed)

	for button in $vbox_left.get_children():
		button.focus_entered.connect(_button_focus_entered.bind(button))

func _load_changelog():
	var text = FileAccess.open("res://CHANGELOG.txt", FileAccess.READ).get_as_text()
	$changelog_panel/margin/changelog.text = text + "\n\n"

func _input(event):
	if event is InputEventKey and event.is_action_pressed("quit"):
		Net.disconnect_from_game()
		await get_tree().create_timer(0.5).timeout
		get_tree().quit()

func _set_buttons_disabled(disabled = true):
	$vbox_left/button_play.disabled = disabled
	$vbox_left/button_join.disabled = disabled
	$vbox_left/button_logout.disabled = disabled

func _button_play_pressed():
	_set_buttons_disabled()
	host_game.emit()

func _button_join_pressed():
	_set_buttons_disabled()
	join_game.emit()

func _button_logout_pressed():
	_set_buttons_disabled()
	await W4GD.auth.logout().async()
	PlayerConfig.user_pass = ""
	PlayerConfig.save()
	back_to_main_menu.emit()

func _button_credits_pressed():
	if $changelog_panel/margin/credits.visible:
		$changelog_panel/margin/credits.hide()
		$changelog_panel/margin/changelog.show()
	else:
		$changelog_panel/margin/credits.show()
		$changelog_panel/margin/changelog.hide()
		

func _button_focus_entered(button):
	# If the button is currently hovered, ignore everything else.
	if Rect2(Vector2(), button.size).has_point(button.get_local_mouse_position()):
		last_focused_button = button
		return

	# If the focus just left the changelog,
	# try to focus the button that was focused before the changelog.
	if changelog_was_focused:
		changelog_was_focused = false
		last_focused_button.grab_focus()
		return
	
	# If this button was just focused and is disabled,
	# try to find the next button to focus that is not disabled.
	if button.disabled:
		if button.get_index() > last_focused_button.get_index():
			# Moving down
			var next = button.find_next_valid_focus()
			if next:
				next.grab_focus()
				return
		else:
			# Moving up
			var prev = button.find_prev_valid_focus()
			if prev:
				prev.grab_focus()
				return

	last_focused_button = button

func _handle_changelog_focus():
	@warning_ignore("shadowed_variable_base_class")
	var has_focus = $changelog_panel/margin/changelog.has_focus()
	if has_focus:
		changelog_was_focused = true

	$changelog_panel/margin/changelog.focus_mode = FOCUS_ALL

	# Fix for changelog parent container needing to display visual focus
	if has_focus:
		if not changelog_focus_style:
			_create_changelog_focus_style()
		$changelog_panel.add_theme_stylebox_override("panel", changelog_focus_style)
	else:
		$changelog_panel.remove_theme_stylebox_override("panel")

func _create_changelog_focus_style():
	var r = RichTextLabel.new()
	var t = r.get_theme_stylebox("focus")
	changelog_focus_style = $changelog_panel.get_theme_stylebox("panel").duplicate()
	changelog_focus_style.border_width_left = t.border_width_left
	changelog_focus_style.border_width_right = t.border_width_right
	changelog_focus_style.border_width_bottom = t.border_width_bottom
	changelog_focus_style.border_width_top = t.border_width_top
	changelog_focus_style.border_color = t.border_color
	r.queue_free()
