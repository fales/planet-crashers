extends Node2D
class_name Character
## Base Player character
## 
## Im considering whether this will be extended by the two types of players
## or if it'll just be a composition of visuals, attach, special and mini_special scenes so that we
## can mix and match attacks and powers as we like for easier prototyping

# STATS

## Maximum hitpoints
@export var MAX_HP : int = 100

## Maximum special points. Use to launch special and mini-special attacks
@export var MAX_SP : int = 120

## Speed in pixels per second
@export var SPEED : float = 3 * Globals.PIXELS_PER_UNIT

## Attack equipped by default
@export var equipped_attack : SkillDB.Weapons = -1:
	set = equip_attack
## Mini-special equipped by default
@export_file("*.tscn", "*.scn") var equipped_mini_special : String = "":
	set = equip_mini_special
## Special equipped by default
@export_file("*.tscn", "*.scn") var equipped_special : String = "":
	set = equip_special

@export var equipped_effect : SkillDB.Effects = SkillDB.Effects.NORMAL:
	set = equip_effect

@export var player_name : String = "Crasher":
	set(val):
		player_name = val
		if !_is_ready:
			await ready
		$name_tag/label.text = player_name

## Direction the character is moving. Exported only for sync purposes
@export var direction : Vector2 = Vector2.ZERO

## Direction the player intends to aim. Exported only for sync purposes
@export var target_aim : Vector2 = Vector2.RIGHT

@export var color : Colors = Colors.RED:
	set(val):
		color = val
		color_suffix = COLOR_SUFFIXES[val]

enum Colors { RED, BLUE, PURPLE, YELLOW, GREEN, YELLOWRED }
const COLOR_SUFFIXES := {
	Colors.RED : "_red",
	Colors.BLUE : "_blue",
	Colors.PURPLE : "_purple",
	Colors.YELLOW : "_yellow",
	Colors.GREEN : "_green",
	Colors.YELLOWRED : "_yellowred",
}

@export var COLORS_RGBA := {
	Colors.RED : Color("f65d43"),
	Colors.BLUE : Color("bec3df"),
	Colors.PURPLE : Color("793a80"),
	Colors.YELLOW : Color("fffc40"),
	Colors.GREEN : Color("d6f264"),
	Colors.YELLOWRED : Color("ffd541"),
}

var color_suffix = "_red"

## Direction the character is aiming. Always of magnitude 1. Use for directed attacks. 
var aim : Vector2 = target_aim

## Reference to basic attack
var attack : Skill = null
## Reference to mini_special attack
var mini_special : Skill = null
## Reference to special attack
var special : Skill = null

## Set to true on left stick input, set to false on keyboard input. Used to handle aiming
var using_gamepad : bool = false

## If true the character always aims in the direction of motion and can be controlled
## On keyboard it means you can aim without a mouse
## On gamepad it means you move and aim with the left stick
var combined_move_and_aim : bool = false

var is_controllable : bool = false

var _is_ready := false

## Current hitpoints
var hp : int = MAX_HP:
	set(value):
		hp = clamp(value, 0, MAX_HP)
		if !_is_ready:
			await ready
		hp_bar.value = hp

## Current special points
var sp : int = MAX_SP:
	set(value):
		sp = clamp(value, 0, MAX_SP)
		if !_is_ready:
			await ready
		sp_bar.value = sp

var dead := false
var game_in_progress := false

@onready var hp_bar : PointBar = $hp_bar
@onready var sp_bar : PointBar = $sp_bar
@onready var visuals : Node2D = $visuals
@onready var sprite : AnimatedSprite2D = $visuals/Sprite

const explosion_scene := preload("res://fx/explosion.tscn")
var names = preload("res://characters/character_names.gd").new().names

var spawn_offset = Globals.LEVEL_SIZE / 2
var spawn_points = [
	# Vector2(Globals.LEVEL_SIZE.x - spawn_offset.x, Globals.LEVEL_SIZE.y - spawn_offset.y), # Center (disabled, used for start game pickup)
	Vector2(Globals.LEVEL_SIZE.x - spawn_offset.x * 1.5, Globals.LEVEL_SIZE.y - spawn_offset.y * 1.5), # Top left
	Vector2(Globals.LEVEL_SIZE.x - spawn_offset.x * 0.5, Globals.LEVEL_SIZE.y - spawn_offset.y * 1.5), # Top right
	Vector2(Globals.LEVEL_SIZE.x - spawn_offset.x * 1.5, Globals.LEVEL_SIZE.y - spawn_offset.y * 0.5), # Bottom left
	Vector2(Globals.LEVEL_SIZE.x - spawn_offset.x * 0.5, Globals.LEVEL_SIZE.y - spawn_offset.y * 0.5) # Bottom right
]

func _enter_tree():
	if str(name).is_valid_int():
		set_multiplayer_authority(str(name).to_int())
	visible = false
	$aim.visible = false
	if not is_multiplayer_authority():
		return
	is_controllable = true
	$aim.visible = true
	if not PlayerConfig.has_user_name():
		player_name = names.pick_random()
	else:
		player_name = PlayerConfig.get_user_name()
	position = spawn_points.pick_random()

func _ready() -> void:
	visible = true
	_is_ready = true
	color = Colors.values().pick_random()
	$aim.self_modulate = COLORS_RGBA[color]
	$aim.z_index = Globals.IN_GAME_UI_Z_INDEX
	$name_tag.z_index = Globals.IN_GAME_UI_Z_INDEX
	$text_baloon.hide()
	_update_params()
	_update_aim()
	GameConfig.updated.connect(_update_params)
	Events.game_begin.connect(_game_started)
	Events.game_over.connect(_game_over)


func _game_started() -> void:
	game_in_progress = true
	announce_dimiss()
	# Reset multiplier
	if attack:
		(attack.get_node("dmg_up") as Timer).start()


func _game_over(players : int, players_alive : int, round : int) -> void:
	game_in_progress = false
	if not is_multiplayer_authority():
		await get_tree().create_timer(5).timeout
		queue_free()
		return
	if hp > 0 and players_alive > 0:
		announce.rpc(["Victory is ours!", "Mission accomplished", "Take that, drones!", "Another day, another win"].pick_random())
	await get_tree().create_timer(5).timeout
	queue_free()


func _physics_process(delta: float) -> void:
	#Handle player actions
	if is_controllable and is_multiplayer_authority():
		if Input.is_action_just_pressed("attack"):
			_attack.rpc()
		if Input.is_action_just_pressed("special"):
			_special.rpc()
		if Input.is_action_just_pressed("mini_special"):
			_mini_special.rpc()
		
		direction = Input.get_vector("move_left","move_right","move_up","move_down").normalized()
		if combined_move_and_aim:
			target_aim = direction
		else:
			target_aim = (Input.get_vector("aim_left", "aim_right", "aim_up", "aim_down") if using_gamepad else get_local_mouse_position()).normalized()
		position += SPEED * direction * delta
		position = position.clamp(
			Vector2(Globals.LEVEL_PLAY_AREA.x, Globals.LEVEL_PLAY_AREA.y),
			Vector2(Globals.LEVEL_PLAY_AREA.z, Globals.LEVEL_PLAY_AREA.w)
		)
	

	if not direction == Vector2.ZERO:
		sprite.play("walk" + color_suffix)
	else:
		sprite.play("idle" + color_suffix)
	_update_aim()
	_update_facing()


func _update_aim():
	if target_aim == Vector2.ZERO:
		return
	if not using_gamepad and combined_move_and_aim:
		aim = aim.slerp(target_aim, 0.2)
	else:
		aim = target_aim
	$aim.position = visuals.position + aim * 1.2 * Globals.PIXELS_PER_UNIT
	$aim.rotation = aim.angle()


func _update_facing():
	var facing : float = sign(aim.x)
	if facing != 0:
		visuals.scale.x = facing

func _on_attack_available():
	#_attack.rpc() # button activated now
	pass	


@rpc("any_peer", "call_local", "reliable")
func _attack():
	if !attack or !attack.is_available:
		return
	attack.launch()


## Launches the mini-special attack. It's an attack that only consumes a fraction of the total SP
@rpc("any_peer", "call_local", "reliable")
func _mini_special():
	if !mini_special:
		return
	# Everything here is placeholder code just to see something on screen
	if sp < mini_special.SP_COST:
		print(name + ": Not enough SP for mini special")
		return
	if !mini_special.is_available:
		print(name + ": Mini Special not ready")
		return
	#sp -= mini_special.SP_COST
	sp_damage(mini_special.SP_COST)
	assert(sp >= 0)
	mini_special.launch()


## Launches the special attack. It's an attack that requires a full SP bar and consumes it completely
@rpc("any_peer", "call_local", "reliable")
func _special():
	if !special:
		return
	# Everything here is placeholder code just to see something on screen
	if sp != MAX_SP:
		print(name + ": Not enough SP for special")
		return
	if !special.is_available:
		print(name + ": Mini Special not ready")
		return
	#sp = 0
	sp_damage(MAX_SP)
	print(name + ": Special")
	special.launch()


## Called by anything wanting to hurt the character
## Negative amounts heal. This is how FF games handle it so you can turn any attack into a heal but it could be changed 
## to modify_hp or something like that so it's more descriptive
func damage(amount : int, custom_data:= {}):
	_take_damage.rpc(amount, custom_data)


@rpc("any_peer", "call_local", "reliable")
func _take_damage(amount : int, _custom_data:= {} ):
	if hp <= 0:
		return

	hp = clamp(hp - amount, 0, MAX_HP)
	if is_multiplayer_authority() and hp <= 0:
		die.rpc()

	var modulate_color = Color(3,0,0) if amount > 0 else Color(0,3,0)
	if "direction" in _custom_data:
		position += remap(amount, 0, 30, 0, 0.5) * Globals.PIXELS_PER_UNIT * _custom_data["direction"]
		position = position.clamp(
				Vector2(Globals.LEVEL_PLAY_AREA.x, Globals.LEVEL_PLAY_AREA.y),
				Vector2(Globals.LEVEL_PLAY_AREA.z, Globals.LEVEL_PLAY_AREA.w)
			)
	var _damage_numbers = DamageNumber.new(get_parent(), position + Vector2.UP * Globals.PIXELS_PER_UNIT, abs(amount), modulate_color)
	visuals.modulate = modulate_color
	await get_tree().create_timer(0.1).timeout
	visuals.modulate = Color.WHITE


@rpc("authority", "call_local", "reliable")
func die():
	if dead:
		return
	dead = true
	is_controllable = false
	print(player_name + " has died")
	set_physics_process(false)
	sprite.play("idle" + color_suffix)
	hp_bar.hide()
	sp_bar.hide()
	$aim.hide()
	if attack:
		attack.hide()
	if mini_special:
		mini_special.hide()
	if special:
		special.hide()
	player_name = "☠" + player_name + "☠"
	for i in 10:
		var boom = explosion_scene.instantiate()
		boom.speed_scale = 4
		boom.position = visuals.position + Vector2(randf_range(-16,16),randf_range(-16,16))
		boom.scale = Vector2.ONE * randf_range(0.25, 0.75)
		visuals.modulate = visuals.modulate.darkened(0.2)
		add_child(boom)
		await get_tree().create_timer(0.1).timeout
	var boom = explosion_scene.instantiate()
	visuals.hide()
	if has_node("shadow"):
		$shadow.hide()
	boom.position = visuals.position
	boom.scale = Vector2(2,2)
	add_child(boom)


## Called by anything wanting to hurt the character SP
## Negative amounts heal. This is how FF games handle it so you can turn any attack into a heal but it could be changed 
## to modify_sp or something like that so it's more descriptive
func sp_damage(amount : int):
	_take_sp_damage.rpc(amount)

@rpc("any_peer", "call_local")
func _take_sp_damage(amount : int):
	sp = clamp(sp - amount, 0, MAX_SP)
	var modulate_color = Color.PURPLE if amount > 0 else Color.CORNFLOWER_BLUE 
	var _damage_numbers = DamageNumber.new(get_parent(), position + Vector2.UP * Globals.PIXELS_PER_UNIT, abs(amount), modulate_color)
	visuals.modulate = modulate_color
	await get_tree().create_timer(0.1).timeout
	visuals.modulate = Color.WHITE


func equip_effect( effect_id : SkillDB.Effects):
	equipped_effect = effect_id
	if !_is_ready:
		await ready
	if attack == null:
		return
	if "effect" in attack:
		attack.effect = effect_id
	else:
		push_warning("Attack doesn't implement effects")

#@rpc("any_peer", "call_local", "reliable")
func equip_attack( weapon_type : SkillDB.Weapons):
#	if equipped_attack:
#		_drop_current_attack()
	# set exported var
	equipped_attack = weapon_type
	var skill_path = SkillDB.weapon_info[weapon_type].scene_path
	if !_is_ready:
		await ready
	# remove current if it exists
	if attack != null:
		#attack.available.disconnect(_on_attack_available) 
		remove_child(attack)
		attack.queue_free()
	# equip new if path is valid
	if skill_path.is_empty():
		return
	attack = load(skill_path).instantiate()
	attack.character = self
	add_child(attack)
	attack.position = visuals.position
	attack.color = color
	if not game_in_progress:
			(attack.get_node("dmg_up") as Timer).stop()
	#DEBUG
	$picked_drop.texture = attack.ICON
	$picked_drop/anim.play("show")
	#attack.available.connect(_on_attack_available) # button activated now

@rpc("any_peer", "call_remote", "reliable")
func _drop_current_attack():
	var drop = preload("res://items/weapon_drop.tscn").instantiate()
	# TODO: Add "entities/drops" node and inject ref to players and enemies
	var drop_offset = (Vector2.DOWN if direction.is_equal_approx(Vector2.ZERO) else -direction) * Globals.PIXELS_PER_UNIT
	drop.global_position = global_position + drop_offset
	get_node("../../enemies").call_deferred("add_child", drop, true)
	# TODO: Create WeaponDB make weapons and effects an enum
	drop.drop_type = equipped_attack
	drop.drop_effect = equipped_effect

#@rpc("any_peer", "call_local", "reliable")
func equip_mini_special( skill_path : String):
	# set exported var
	equipped_mini_special = skill_path
	if !_is_ready:
		await ready
	# remove current if it exists
	if mini_special != null:
		remove_child(mini_special)
		mini_special.queue_free()
		# equip new if path is valid
	if skill_path.is_empty():
		return
	mini_special = load(skill_path).instantiate()
	mini_special.character = self
	@warning_ignore("integer_division")
	sp_bar.subdivisions = MAX_SP / mini_special.SP_COST
	add_child(mini_special)
	mini_special.position = visuals.position
	mini_special.color = color


#@rpc("any_peer", "call_local", "reliable")
func equip_special( skill_path : String):
	# set exported var
	equipped_special = skill_path
	if !_is_ready:
		await ready
	# remove current if it exists
	if special != null:
		remove_child(special)
		special.queue_free()
		# equip new if path is valid
	if skill_path.is_empty():
		return
	special = load(skill_path).instantiate()
	special.character = self
	add_child(special)
	special.position = visuals.position
	special.color = color


@rpc("any_peer", "call_local", "reliable")
func _player_ready():
	if not is_multiplayer_authority() or $text_baloon.visible or game_in_progress:
		return
	print("%s is ready to play" % player_name)
	announce.rpc("[shake]%s[/shake]" % ["Let's go!", "Lock 'n loaded!", "Let's rock!", "Ready to go!", "Ready!"].pick_random(), false)


func announce_dimiss():
	$text_baloon.hide()


@rpc("call_local", "reliable")
func announce(text : String, dismiss := true):
	if len(text) <  1:
		return
	$text_baloon.show()
	var label : RichTextLabel = $text_baloon/label
	label.parse_bbcode(text)
	label.visible_ratio = 0.0
	var tween = get_tree().create_tween()
	tween.tween_property(label, "visible_ratio", 1.0, 1.0)
	tween.tween_interval(1.0)
	if dismiss:
		tween.finished.connect(announce_dimiss)


func _input(event):
	if not is_multiplayer_authority():
		return

	if Input.is_action_just_pressed("ready_to_play"):
		_player_ready.rpc()
	
	#If using mouse, moving the left stick more than half way to the edge activates gamepad mode
	if not using_gamepad and event is InputEventJoypadMotion:
		using_gamepad = abs((event as InputEventJoypadMotion).axis_value) > 0.5 # Account for stick drift
	
	if using_gamepad:
		#Pressing any key on the keyboard activates KB/M mode
		if event is InputEventKey:
			using_gamepad = false
		elif event is InputEventJoypadButton and event.button_index == JOY_BUTTON_A:
			combined_move_and_aim = true
		elif (
					event is InputEventJoypadMotion and
					event.axis in [JOY_AXIS_RIGHT_X,JOY_AXIS_RIGHT_Y] and
					abs(event.axis_value) > 0.5
			):
			combined_move_and_aim = false
	
	if event.is_action_pressed("aim_style_toggle"):
		combined_move_and_aim = !combined_move_and_aim


func _update_params():
	#var old_max_hp = MAX_HP
	MAX_HP = GameConfig.Character_MAX_HP
	#hp = ceil(float(hp) / old_max_hp * MAX_HP)
	hp = MAX_HP
	MAX_SP = GameConfig.Character_MAX_SP
	sp = GameConfig.Character_INITIAL_SP
	SPEED = GameConfig.Character_SPEED * Globals.PIXELS_PER_UNIT
	
	hp_bar.max_value = MAX_HP
	hp_bar.value = hp
	@warning_ignore("narrowing_conversion")
	hp_bar.warning_below = 0.2 * MAX_HP # flash below 10%
	
	sp_bar.max_value = MAX_SP
	sp_bar.value = sp
	sp_bar.warning_above = MAX_SP-1 # flash on full bar

@rpc("any_peer", "reliable", "call_local")
func request_despawn():
	queue_free()
