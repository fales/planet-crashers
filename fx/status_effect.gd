@tool
extends Sprite2D

const TEXTURES := {
	Enemy.Statuses.NORMAL : null,
	Enemy.Statuses.CRYONIZED : preload("res://assets/sfx/ui/status_cryo.png"),
	Enemy.Statuses.CORRODING: preload("res://assets/sfx/ui/status_acid.png"),
	Enemy.Statuses.BLAZED: preload("res://assets/sfx/ui/status_pyro.png"),
}

@export var type := Enemy.Statuses.NORMAL:
	set(v):
		type = v
		texture = TEXTURES[v]
