extends Node2D

var player_ref : Character = null

const ALPHA_AVAILABLE = 0.6
const ALPHA_UNAVAILABLE = 0.2

func _ready() -> void:
	# Attacks have really small cooldowns so always show as available otherwise the blinking
	# can become too annoying
	$attack.self_modulate.a = ALPHA_AVAILABLE

# TODO: use signals
func _process(delta: float) -> void:
	if !player_ref:
		return
	var mini_special_launchable = player_ref.sp >= player_ref.mini_special.SP_COST and player_ref.mini_special.is_available
	var special_launchable = player_ref.sp == player_ref.MAX_SP and player_ref.special.is_available
	$cyclone.self_modulate.a = ALPHA_AVAILABLE if mini_special_launchable else ALPHA_UNAVAILABLE
	$surge.self_modulate.a = ALPHA_AVAILABLE if special_launchable else ALPHA_UNAVAILABLE
