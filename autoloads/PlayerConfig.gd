## Autoload for user settings.
## Any properties not starting with an underscore are auto persisted to and from disk.
## Properties should have a setter to apply their value to the game when set.
## Values not intended to be changed by the user should be stored in the Globals autoload.
##
## To use different profiles for multiple clients in multiplayer testing, pass "--client=<profile>",
## as a command line parameter when starting the game, for example "--client=2".
extends Node

# DEFINE USER SETTINGS BELOW
# ------------------------------------------------------------------------------

# Audio
var audio_mute := false:
	set(v):
		audio_mute = v
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), v)

# User
var user_email := ""
var user_pass := ""
var user_store := true

# Networking
var net_max_players := 4

# ------------------------------------------------------------------------------
# END OF USER SETTINGS

const _settings_version = 1
const _settings_prefix = "user://settings"
const _settings_ext = ".cfg"
var _settings_defaults = {}

func has_user_name() -> bool:
	return len(user_email) > 0 and user_email.contains("@")

func get_user_name() -> String:
	if not has_user_name():
		return ""
	return user_email.rsplit("@", false, 1)[0]

func get_settings_path():
	return _settings_prefix + Globals.get_cli_arg("client") + _settings_ext

## Loads settings from disk.
@warning_ignore("shadowed_global_identifier")
func load() -> Error:
	var config = ConfigFile.new()
	print("Loading: ", get_settings_path())
	var err = config.load(get_settings_path())
	if err != OK:
		return err

	if config.get_value("config", "version", -1) != _settings_version:
		return ERR_INVALID_DATA

	var data = config.get_value("config", "data", {})
	if typeof(data) != TYPE_DICTIONARY or data.size() < 1:
		return ERR_FILE_CORRUPT

	for key in data:
		set(key, data[key])

	return OK

## Saves settings to disk.
func save(data=_to_dict()) -> Error:
	var config = ConfigFile.new()
	config.set_value("config", "version", _settings_version)
	config.set_value("config", "data", data)
	print("Saving: ", get_settings_path())
	return config.save(get_settings_path())

## Resets all settings to default values (even those stored on disk).
func reset_to_default() -> Error:
	var err := save(_settings_defaults)
	if err != OK:
		return err
	return self.load()

func _ready():
	_settings_defaults = _to_dict()
	self.load()
	save()

func _to_dict() -> Dictionary:
	var out := {}
	var base := _get_base_properties()
	for property in get_property_list():
		if property.name.begins_with("_") or \
			property.name.ends_with(".gd") or \
			property.name in base:
			continue
		out[property.name] = get(property.name)
	return out

func _get_base_properties() -> Dictionary:
	var base = Node.new()
	var props = {}
	for property in base.get_property_list():
		props[property.name] = true
	base.free()
	return props
