# Planet Crashers

Download and play on Itch.io: <https://w4games.itch.io/planet-crashers>

A small demo game to show off W4 Games cloud services and networking features in Godot Engine 4.

For local testing, start a server instance with `--headless`, then have other clients join using the
IP address (or by leaving the field empty for localhost) and clicking "Join" in the join menu.

For online testing, multiple accounts must be created and used to test with more than one player.
You can use the `--client=<profile>` CLI parameter to start the game multiple times with different
settings files (and thus, login with different accounts). For example, you could start one instance
normally, and one with `--client=2`, then have one host and one join that cloud game, or have both
join a cloud game created by a third user.

## Technical Info

### W4 Games Cloud networking SDK (`addons/W4GD/`)

Used to connect, query, host and join cloud-hosted multiplayer games, and to interact with W4 services.

### GameConfig singleton

Contains a list of gameplay parameters.

`signal updated()`: emitted whenever the parameters are updated.
Classes can connect to this signal to update the actual variables involved as soon as possible (eg: player speed, max health, etc.)

### Net singleton

Contains utility functions related to networking like `host_game()`, `join_game()`, etc.

### PlayerConfig singleton

Defines settings meant to be persisted to and from disk. E.g: `user_email`, `net_port`, `audio_mute`

### Events singleton

Contains global events like `game_begin()`, `game_over()`, `round_begin()`, `round_end`, etc.

### Globals singleton

A place for any constants, variables or functions that need to be available globally and don't have a more specific location yet. Ideally it should contain only read-only data and be avoided for anything that could have a smaller scope

### Character class

Represents a playable character. 

#### Stats

`@export var MAX_HP`: Maximum hit points

`@export var MAX_SP`: Maximum special points. Used by mini-special and special attacks

`@export var SPEED`: Movement speed in pixels per second. There is no acceleration or deceleration to make movement as snappier as possible.

`var hp`, `var sp`: current values for hp and sp

#### Skill Slots

Three slots where any skill (see Skill class) can be equipped both in the editor and at runtime. Each serves a different purpose

`@export var default_attack`: This is the basic attack the player starts with and can be used at 0 SP cost regardless of the cost defined in the associated Skill scene.

`@export var default_mini_special`: This is the mini special the player starts with. The SP cost is defined inside the associated Skill scene (see Skill class). It should be more situational and powerful than the base attack

`@export var default_special`: This is the special move/ultimate ability the player starts with. This skill will always require and consume a full SP bar regardless of the cost in the associated Skill scene

`var attack`, `var mini-special`, `var special`: variables of type Skill holding a reference to the currently equiped skills. Can be modified with the `equip_*` methods.

#### Damage and Heal

`damage(amount: int, custom_data:={})`: Can be used to damage the player or heal it(using negative values). `custom_data` is an optional dictionary that can contain any info. It's up to the class implementing damage to check for any relevant info and handle it accordingly. Could be used for example to pass a damage type (burning, freezing, shocking, etc.)

`sp_damage(amount)`: Same as damage but affecting special points instead of hit points. Mostly used by pickups that recharge SP. Doesn't take any custom data for now. 

#### Character Skins (colors) and Name Tag

There is a `color` variable and a `Colors` enum with the values `RED`, `BLUE`, `PURPLE`, `YELLOW`, `GREEN`, `YELLOWRED`
This value is mapped to sprite sets or actual color values and used to differentiante players and their attacks.

`@export var player_name` can be used to set the name tag on top of each player.

#### Controls

Characters can be controlled with a gamepad or a keyboard/mouse combo. The game automatically switches between one or the other based on last type of input, properly accounting for possible analog drift/noise.

Two control schemes are supported:

- Separate move and aim, aka "twin stick",  aka "KB/M"(deafault):
  - On gamepad move with the left stick and aim with the right stick.
  - On keyboard move with WASD and aim with the mouse
- Combined move and aim: Same as before but the player always aims in the direction of motion. This can be useful as a simpler control scheme, for gamepads with only one stick, for keyboard only gameplay or as an accesibility feature. 

Default controls:

- WASD / Left Stick: Move
- Shift, Left Trigger, RMB: Mini-special
- Space, KP0, Left Shoulder: Special
- Ctrl, Right Trigger, LMB: Attack
- Mouse position, Right Stick: Aim
- Tab, Top Action Button: Aim style toggle
- ESC, Select: Quit
- Enter, Start: Ready to play, Start match (host only)

### Skill class

Skills are any kind of attack or move that the player can perform. There is no distinction between a skill meant to be used as a base attack, mini-special or special. An basic attack deemed too OP by the game designer can be simply moved to the mini-special role. A skill that was equippable as a special early in the game could be equippable as a mini-special later.

#### Public properties and methods:

- `@export var DISPLAY_NAME`: display name if the Skill. Meant for UI. Currently unused
- `@export var SP_COST`: cost per use in special points. Used by the mini-special slot
- `@export var COOLDOWN`: time between uses. Cooldown starts as soon as the attack is launched
- `var cooldown_remaining`: remaining cooldown time
- `var is_available`: true if `cooldown_remaining` is 0
- `signal available`: emitted when the cooldown timer reaches 0
- `launch()`: launches the attack and starts the cooldown timer. The actual behavior is defined in child classes by overriding `_launch()`
- `character` : reference to the Character that has this skill equipped [deprecated]

### Hitbox class

A ShapeCast2D node that can deal damage. Used mostly for attacks that need immediate collision overlaps, such as slashes. 

- `@export var is_enemy`: true if it's a hitbox meant for an enemy, false if it's meant for a player or ally npc. The setter adjusts the collision_mask accordingly
- `deal_damage( amount: int, custom_data:= {} ) -> int`:  will update it's collision and call `damage()` on every object it collides with. Returns the target count

### HitboxArea class

Same as Hitbox but using a Area2D. Useful for attacks that don't need immediate collision overlaps, such as AoE damage. Contains the same properties and methods as the Hitbox class plus the following:

- `var targets`: list of current targets inside the area
- `signal target_aquired( target )`: emitted when a new target enters the area
- `signal target_lost( target )`: emitted when a target leaves the area
- `deal_damage_to_random_target( amount: int, custom_data:= {}) -> bool`: calls `damage()` on a random target, if any. Returns true if successful or false if the target list is empty. 

### Hurtbox class

An Area2D that can be attached to any object that should receive damage. Will check that the owner implements the `damage()` method

### PointBar class

Animated points bar with background, foreground and animation colors. Can be set to flash below and/or above a certain threshold. Currently used to display hp and sp. 

- `@export var bg_color`: background color. Visible on the empty part of the bar.
- `@export var fg_color`: foreground color. Visible on the filled part of the bar.
- `@export var anim_color`: animation color. Used when the bar changes value to better display how much was lost/added
- `@export var max_value`: maximum value. When `value` is equal to `max_value` the bar will be fully painted with `fg_color`
- `@export var value`: the current value of the bar. The proportion of the bar filled with `fg_color` is equal to `value/max_value`
- `@export var size`: Vector2i containing the display size (in pixels) of the bar
- `@export var subdivisions`: contains the amount of subdivisions the bar has. A line of darkened `fg_color` will be drawn between each subdivision.
- `@export var warning_below`: the bar will start flashing when `value` is smaller. This is an absolute value, not a percentage of `max_value`.
- `@export var warning_above`: the bar will start flashing when `value` is larger. This is an absolute value, not a percentage of `max_value`. 

### Enemy class

Base class for enemies.

#### Stats

- `@export var MAX_HP`: Maximum hitpoints
- `@export var SPEED`: move speed in pixels per second
- `@export var ATTACK_RANGE`: attack radius in pixels. Enemies should not attack anything outside it's radius. Inside the radius it's up to each enemy implementaion whether it is interpreted as "can", "should" or "must" attack. 
- `@export var SP_DROP_AMOUNT`: how much SP it drops on death. All enemies can drop SP charges ("powercells") on death
- `@export var HEALTHBAR_VISIBLE`: whether the health bar is visible or not. Usually only set to true for enemies that can withstand several attacks

## License

Game code and the W4 Games addon (`addons/W4GD/*`) are licensed under the MIT license, see `LICENSE.txt`
unless otherwise noted. Original assets are licensed under Creative Commons CC-BY 4.0.
Third party code and assets are used under the terms of their respective licenses,
see `CREDITS.ini` or the in-game credits menu.
